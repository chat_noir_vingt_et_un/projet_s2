# Projet S2

Known bugs (last review : 25/02/2020):

  - when taking damge all health bars reflect the user's health (only tested with 2 players)
      further testing is required

  - your own health bar and name is visible at your feet 
      I thought of an easy fix already but didn't have time to try to implement

  - jumping and moving forward while being right next to an object will fail
      this is caused by the colliders of the player, notable the groundcheck and player body
      player collider has a capsule shap while ground check is a rectangle, 
      ground check is sometimes triggered when the collider is not yet on the ground

  - cannot jump while running down stairs
      I suspect the gravity is not strong enough or the player speed is too high
      As such the player just "glides" over the stairs and never triggers isGounded, therefore losing the ability to jump


Changelog :

-- 25/02/2020 --
11h50
   Added a health bar and player names, they appear above the othert players (subject to change)
   Added a quit button
   minor quality of life changes :
      cursor is invisible while in game
      made sure cursor is visible within the menu

-- 24/02/2020 --
11h50
   Bug Fixed : multiple instanciantion of players per room when a new user joins
      also fixed a teleporting issue when a player joined the room
11h
   Enabled Multiplayer, players can now join and control their own character

-- 23/02/2020 --
19h
   Added Health, press "f" to lose 0.5 health, you will disconnect if your health goes down to zero or less
   if you press "z" your cursor will be free, your game camera will now stay still when you are in this pause state

18h
   some features have been reverted due to the merge failure, I will try to patch all of them before long.
   bugs are still there, however we can now connect and disconnect once more.

-- 15/02/2020 -- 

   The player is now stopped by ceilings
   A few tweaks to player have been made however a few bugs still remain


-- 05/02/2020 -- 

9h45
  The bug where you would reconnect automatically to a game after disconnect is resolved

-- 05/02/2020 --

20h15
  A Functionnal version of a menu has been implemented, the player can choose the name he wants to go by
  After inputting a name the player can click on the Connect button, it will start the connection process
  While connecting the player will see the connect button and input field disappear and be replaced by a "Connecting text"

23h00
  The "lobby" scene will be loaded when the client connects to the master client
  The leave room button is WIP, it will automatically reconnect you after leaving the room...

-- pre 02/05/2020 --

Note on the application of gravity on the player :
 
 The player is grounded when a object of the layer "Ground" comes whithin a 0.5 radius of his feet
 Gravity is applied on the player when he is not grounded.
 When implementing terrain, do NOT forget to make them part of the "Ground" layer

 PS : "Ground" layer might change to "Environment" for clarity's sake
