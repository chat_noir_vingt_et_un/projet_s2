﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class wait : MonoBehaviour
{

    public Animator transition;

    public float waiting_time;

    public float transition_time;

    void Start()
    {
        StartCoroutine(WaitForIntro());
    }

    IEnumerator WaitForIntro()
    {
        yield return new WaitForSeconds(waiting_time);
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transition_time);

        SceneManager.LoadScene(0);
    }
}
