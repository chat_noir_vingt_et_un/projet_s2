﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btnFX : MonoBehaviour
{

    public AudioSource myFX;
    public AudioClip hover;
    public AudioClip click;

    public void HoverSound()
    {
        myFX.PlayOneShot(hover);

    }

    public void ClickSound()
    {
        myFX.PlayOneShot(click);

    }

}
