﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class transition_in : MonoBehaviour
{

    public Animator transition;

    public float transition_time = 1f;

    void Start()
    {
        //StartCoroutine(WaitForTransition());


    }

    IEnumerator WaitForTransition()
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transition_time);
    }
}
