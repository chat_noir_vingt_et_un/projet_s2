﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispAtAwake : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var r = GetComponent<SkinnedMeshRenderer>();
        r.enabled = true;
    }
}
