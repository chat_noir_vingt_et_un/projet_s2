﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhyDoIHearBossMusic : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public AudioSource world;
    [SerializeField]
    public AudioSource boss;
    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.CompareTag("Player"))
        {
            world.Stop();
            boss.Play();
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
