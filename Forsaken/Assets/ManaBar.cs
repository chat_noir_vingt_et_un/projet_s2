﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{
    public float mana = 100;
    
    private float cd;

    private Image circle;

    private Image bar;

    public float cost;
    
    // Start is called before the first frame update
    void Start()
    {
        mana = 100;
        cost = 12.5f;
        circle = transform.Find("circle").GetComponent<Image>();
        bar = transform.Find("bar").GetComponent<Image>();
    }

    private void Update()
    {
        cd += Time.deltaTime;
        if (Input.GetKeyUp(KeyCode.Mouse1) && cost <= mana)
        {
            cd = 0;
            if (mana > 50)
            {
                cost = 12.5f;
                mana -= cost;
            }
            else
            {
                cost = 6.25f;
                mana -= cost;
            }
        }
        if (cd > 1 && mana < 100)
        {
            if (mana > 50)
            {
                mana += 0.5f;
            }
            else
            {
                mana += 0.25f;
            }
        }
        
        bar.fillAmount = (mana - 50) / 50;
        circle.fillAmount = mana / 50;
    }
}
