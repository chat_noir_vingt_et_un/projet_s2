﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsWindow : MonoBehaviour
{

    public static bool SettingsIsOn = false;

    public GameObject SettingsWindowUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SettingsIsOn)
            {
                Resume();
            }
            else
            {
                DisplaySettings();
            }
        }

        
    }

    public void Resume()
    {
        SettingsWindowUI.SetActive(false);
        SettingsIsOn = false;
    }

    public void DisplaySettings()
    {
        SettingsWindowUI.SetActive(true);
        SettingsIsOn = true;
    }

}
