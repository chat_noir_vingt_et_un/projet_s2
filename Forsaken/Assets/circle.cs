﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class circle : MonoBehaviour
{
    private bar b;
    public bool empty;
    private float cooldown;
    public Image i;
    // Start is called before the first frame update
    void Start()
    {
        empty = false;
        i = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        cooldown += Time.deltaTime;
        if (empty)
        {
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                cooldown = 0;
                i.fillAmount -= 0.25f;
            }
        }
        if (i.fillAmount >= 1)
        {
            empty = false;
        }
        if (cooldown >= 2)
        {
            i.fillAmount += Time.deltaTime / 5;
        }
    }
}
