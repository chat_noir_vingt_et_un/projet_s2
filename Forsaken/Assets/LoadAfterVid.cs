﻿using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class LoadAfterVid : MonoBehaviour
{
    public VideoPlayer VideoPlayer; // Drag & Drop the GameObject holding the VideoPlayer component
    public int SceneID;

    void Start()
    {
        VideoPlayer.loopPointReached += LoadSceneAfterVideo;
    }

    void LoadSceneAfterVideo(VideoPlayer vp)
    {
        SceneManager.LoadScene(SceneID);
    }
}