﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class UI_PlayerNames : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI LocalPlayer;
    public TextMeshProUGUI Player2;
    public TextMeshProUGUI Player3;
    public TextMeshProUGUI Player4;

    private void Update()
    {
        LocalPlayer.text = PhotonNetwork.NickName;
        var players = PhotonNetwork.PlayerListOthers;
        switch (players.Length)
        {
            case 0:
                Player2.text = "Waiting . . .";
                Player3.text = "Waiting . . .";
                Player4.text = "Waiting . . .";
                break;
            case 1:
                Player2.text = players[0].NickName;
                Player3.text = "Waiting . . .";
                Player4.text = "Waiting . . .";
                break;
            case 2:
                Player2.text = players[0].NickName;
                Player3.text = players[1].NickName;
                Player4.text = "Waiting . . .";
                break;
            case 3:
                Player2.text = players[0].NickName;
                Player3.text = players[1].NickName;
                Player4.text = players[2].NickName;
                break;
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        var players = PhotonNetwork.PlayerListOthers;
        switch (players.Length)
        {
            case 0:
                Player2.text = "Waiting . . .";
                Player3.text = "Waiting . . .";
                Player4.text = "Waiting . . .";
                break;
            case 1:
                Player2.text = players[0].NickName;
                Player3.text = "Waiting . . .";
                Player4.text = "Waiting . . .";
                break;
            case 2:
                Player2.text = players[0].NickName;
                Player3.text = players[1].NickName;
                Player4.text = "Waiting . . .";
                break;
            case 3:
                Player2.text = players[0].NickName;
                Player3.text = players[1].NickName;
                Player4.text = players[2].NickName;
                break;
        }
    }
    
    public override void OnPlayerLeftRoom(Player newPlayer)
    {
        var players = PhotonNetwork.PlayerListOthers;
        switch (players.Length)
        {
            case 0:
                Player2.text = "Waiting . . .";
                Player3.text = "Waiting . . .";
                Player4.text = "Waiting . . .";
                break;
            case 1:
                Player2.text = players[0].NickName;
                Player3.text = "Waiting . . .";
                Player4.text = "Waiting . . .";
                break;
            case 2:
                Player2.text = players[0].NickName;
                Player3.text = players[1].NickName;
                Player4.text = "Waiting . . .";
                break;
            case 3:
                Player2.text = players[0].NickName;
                Player3.text = players[1].NickName;
                Player4.text = players[2].NickName;
                break;
            
        }
    }
}
