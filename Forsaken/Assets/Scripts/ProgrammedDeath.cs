﻿using UnityEngine;

public class ProgrammedDeath : MonoBehaviour
{
    public int TimeToLive;
    public GameObject ToDestroy;
    
    // Start is called before the first frame update
    void Start()
    {
        Destroy(ToDestroy,TimeToLive);
    }
}
