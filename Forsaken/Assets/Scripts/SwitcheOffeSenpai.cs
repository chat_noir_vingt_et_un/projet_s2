﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.VFX;

public class SwitcheOffeSenpai : MonoBehaviour
{
    private VisualEffect FireThing;
    private Light BrightStuff;
    private SphereCollider col;
    private bool on;
    private LightoFickeru l;
    private LayerMask _layerMask;
    private GameObject[] players;

    private void Start()
    {
        _layerMask = LayerMask.GetMask("Player", "Ground");
    }

    void Awake()
    {
        FireThing = transform.GetChild(0).GetComponent<VisualEffect>();
        BrightStuff = transform.GetChild(1).GetComponent<Light>();
        l = transform.GetChild(1).GetComponent<LightoFickeru>();
        col = GetComponent<SphereCollider>();
        FireThing.enabled = false;
        BrightStuff.enabled = false;
        on = false;
        players = GameObject.FindGameObjectsWithTag("Player");
    }

    private void SwitchOn()
    {
        on = true;
        BrightStuff.enabled = true;
        l.overrideTarget = false;
        l.enabled = true;
        FireThing.enabled = true;
    }

    private void SwitchOff()
    {
        on = false;
        l.overrideTarget = true;
    }
    private void Update()
    {
        foreach (var p in GameObject.FindGameObjectsWithTag("Player"))
        {
            RaycastHit hit;
            Vector3 pos = transform.position + new Vector3(0, 0.5f, 0);
            Vector3 direction = Vector3.Normalize(p.transform.position + new Vector3(0, 0.5f, 0) - pos);
            Physics.Raycast(pos, direction, out hit, float.MaxValue, _layerMask);
            Debug.DrawRay(pos, direction*hit.distance,  Color.yellow);

            if (hit.collider != null && hit.collider.CompareTag("Player"))
            {
                if (on) return;
                
                SwitchOn();
                return;
            }
        }
        
        if (on) SwitchOff();
    }
}
