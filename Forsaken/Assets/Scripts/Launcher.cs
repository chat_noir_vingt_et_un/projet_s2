﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;


namespace Tweleyes.Forsaken
{

    public class Launcher : MonoBehaviourPunCallbacks
    {
        #region private serializable fields

        [Tooltip("The max numbers of player per room, when a room is full new players cannot join it")] [SerializeField]
        private byte maxPlayersPerRoom = 4;
        
        [Tooltip("The Ui Panel to let the user enter name, connect and play")] [SerializeField]
        private GameObject controlPanel;
        
        [Tooltip("Text that informs the player that the connection is in progress")] [SerializeField]
        private GameObject connectProgress;

    #endregion

        
        #region Private Fields

        // Users are separated by game version
        private string Gameversion = "1";

        /// <summary>
        /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon,
        /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
        /// This is used for the OnConnectedToMaster() callback.
        /// </summary>
        private bool isConnecting;

        #endregion


        #region Monobehavior Callbacks

        void Awake()
        {
            // CRUCIAL !
            // Allows the scenes to be loaded on all clients at the same time when LoadScene() is used by the master client
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            connectProgress.SetActive(false);
            controlPanel.SetActive(true);
        }

        #endregion

        
        #region Pun Callbacks
        public override void OnConnectedToMaster() 
        {
            //Debug.Log("OnConnectedToMaster() has been called");
            
            // we don't want to do anything if we are not attempting to join a room.
            // the cases where isConnecting is false is when you lost or quit the game, when this level is loaded, OnConnectedToMaster will be called,
            //  in that case we don't want to do anything.
            if (isConnecting)
            {
                // Checks for a free room if one exists, if there is none then we will callback OnJoinRandomRoomFailed()
                PhotonNetwork.JoinRandomRoom();
                isConnecting = false;
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            //Debug.Log("OnJoinRandomRoomFailed() was called : no available room \nCalling CreateRoom()");
            PhotonNetwork.CreateRoom(null, new RoomOptions{MaxPlayers = maxPlayersPerRoom});
        }

        public override void OnJoinedRoom()
        {
            //Debug.Log("OnJoinedRoom() was called, the client is now in a room");
            // #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                //Debug.Log("We load the Lobby ");
                
                // #Critical
                // Load the Room Level.
                PhotonNetwork.LoadLevel("WaitingRoom");
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            connectProgress.SetActive(false);
            controlPanel.SetActive(true);
            //Debug.LogWarningFormat("OnDisconnected() was called with cause : {0}", cause);
        }

        #endregion

        
        #region Public Methods

        /// <summary>
        /// Start the connection process.
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
            //Debug.Log("Connect() has been called");
            // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
            isConnecting = true;
            
            connectProgress.SetActive(true);
            controlPanel.SetActive(false);
            
            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.ConnectUsingSettings(); // Crucial : Before anything we need to be connected to the Photon Server
                PhotonNetwork.GameVersion = Gameversion;
            }
            else
            {
                PhotonNetwork.JoinRandomRoom();
            }
        }

        public void PlayOffline()
        {
            PhotonNetwork.OfflineMode = true;
            PhotonNetwork.LoadLevel("SampleScene");
        }

        /// <summary>
        /// Will load the playing field for the players :
        /// </summary>
        public void StartToPlay()
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.LoadLevel("SampleScene");
        }

        public void Quit()
        {
            Application.Quit();
        }

        #endregion
    }
}
