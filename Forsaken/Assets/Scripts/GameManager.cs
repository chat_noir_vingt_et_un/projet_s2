﻿using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Map.WFC;
using Object = UnityEngine.Object;

namespace Tweleyes.Forsaken
{
    public class GameManager : MonoBehaviourPunCallbacks, IPunObservable
    {
        #region Public Fields

        public static GameManager Instance;
        
        [Tooltip("The prefab to use for representing the player")]
        public GameObject playerPrefab;

        [Tooltip("Where the player should spawn")]
        public Vector3 spawnPoint;
        
        private int maxTries = 400; // nb of tries for the map generation
        
        private string textout = "";

        private bool mapSent = false;
        private bool mapLoaded = false;
        private bool playerloaded = false;
        
        #endregion

        #region Pun Callbacks
        /// <summary>
        /// Returns the player to the main menu after a disconnect
        /// </summary>
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            //Debug.LogFormat("OnPlayerEnteredRoom(): {0} Joined the room", newPlayer.NickName);
            
            if (PhotonNetwork.IsMasterClient)
            {
                //Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
            }
        }

        public override void OnPlayerLeftRoom(Player other)
        {
            //Debug.LogFormat("OnPlayerLeftRoom(): {0} Left the room", other.NickName); // seen when other disconnects
            
            if (PhotonNetwork.IsMasterClient)
            {
                //Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
            }
        }
        
        #endregion

        #region Monobehavior Callbacks

        public void Awake()
        {
            if (PhotonNetwork.IsMasterClient && !mapSent)
            {
                MapGen m;
                Dictionary<Tnode, Tstate> result = null;
                do
                {
                    m = new MapGen();
                    result = m.Generate(maxTries);
                } while (result == null);
        
                textout = MapGenIO.WriteDefinitionFile(result, m.WorldSize, m.CellSize);
            }
        }

        public void Start()
        {
            Instance = this;
        }

        private void Update()
        {
            if (textout != "" && !mapLoaded)
            {
                //Debug.Log("Loading map");
                MapLoader.Load(textout);
                //Debug.Log("Map loaded");
                mapLoaded = true;
            }
            
            //instances the player 
            //we want this to be done after the map loads
            if (!playerloaded && mapLoaded)
            {
                if ( playerPrefab == null)
                {
                    //Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'",this);
                }
                else
                {
                    if (PlayerManager.LocalPlayerInstance == null)
                    {
                        //Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);
                    
                        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                        //PhotonNetwork.Instantiate(playerPrefab.name, spawnPoint, Quaternion.identity, 0);
                        Utils.MoveToLayer(PhotonNetwork.Instantiate(playerPrefab.name, spawnPoint, Quaternion.identity, 0).transform, LayerMask.NameToLayer("Player"));
                        playerloaded = true;
                    }
                    else
                    {
                        //Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
                    }
                }
            }
        }

        #endregion
        
        #region Public Method

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            PhotonNetwork.LoadLevel("GameOver");
        }

        #endregion

        #region Private Mathods

        public void LoadLobby()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                //Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
            }
            //Debug.LogFormat("PhotonNetwork : Loading Level : SampleScene");
            PhotonNetwork.LoadLevel("SampleScene");
        }

        #endregion

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                if (PhotonNetwork.IsMasterClient && !mapSent)
                {
                    stream.SendNext(textout);
                    mapSent = true;
                }
            }
            else
            {
                if (!mapSent)
                {
                    textout = (string) stream.ReceiveNext();
                    mapSent = true;
                }

            }

        }
    }

}