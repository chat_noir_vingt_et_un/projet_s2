﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using Random = System.Random;

public class LightoFickeru : MonoBehaviour
{
    private Light l;
    private float baz;
    private Random rnd;
    private double target;
    private float time;
    public bool overrideTarget = false;
    private VisualEffect anim;
    

    // Start is called before the first frame update
    void Start()
    {
        l = GetComponent<Light>();
        baz = l.intensity;
        rnd = new Random();
        time = 0;
        anim = transform.parent.GetChild(0).GetComponent<VisualEffect>();
    }

    // Update is called once per frame
    void Update()
    {
        if (overrideTarget && l.intensity < 0.2)
        {
            l.enabled = false;
            anim.enabled = false;
            enabled = false;
            time = 0;
        }
        if (time <= 0)
        {
            if (overrideTarget)
            {
                time = 10;
                target = 0;
            }
            else
            {
                time = (float) (rnd.NextDouble() * 3 + 10);
                target = baz - rnd.NextDouble() * 4;
            }
        }
        else
        {
            l.intensity += (float)((target - l.intensity) / time);
            time -= 1;
        }
    }
}
