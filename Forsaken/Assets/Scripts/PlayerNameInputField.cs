﻿using System;
using UnityEngine;
using Photon.Pun;
using TMPro;



namespace Tweleyes.Forsaken
{
    [RequireComponent(typeof(TMP_InputField))]
    public class PlayerNameInputField : MonoBehaviour
    {

        #region Private Const

        const string playerNamePrefKey = "PlayerName";

        #endregion
        
        #region Monobehavior Callbacks

        private void Start()
        {
            string defaultName = String.Empty;
            TMP_InputField _inputField = GetComponent<TMP_InputField>();
            
            //Debug.Log(defaultName);
            
            if (_inputField != null)
            {
                if (PlayerPrefs.HasKey(playerNamePrefKey))
                {
                    defaultName = PlayerPrefs.GetString(playerNamePrefKey);
                    _inputField.text = defaultName;
                }
                
                //Debug.Log(defaultName);
                
                PhotonNetwork.NickName =  defaultName;
            }
        }

        #endregion

        #region Public Methods

        public void setPlayerName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                //Debug.Log("String is null or empty");
                return;
            }

            PhotonNetwork.NickName = name;
            PlayerPrefs.SetString(playerNamePrefKey, name);
        }

        #endregion
    }
}
