﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    
    private Transform _transform;

    [Tooltip("A reference to the bullet's parent gameObject")] [SerializeField]
    private GameObject bullet;

    [Tooltip("The Speed at which the bullet travels")] [SerializeField]
    private int speed;

    [Tooltip("The lifetime of the bullet, it will be deleted after the time is up")] [SerializeField]
    private float time;


    // Start is called before the first frame update
    void Start()
    {
        Destroy(bullet, time);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime * Vector3.forward);
    }

    public void DestroyBullet()
    {
        Destroy(bullet);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger");
        if (other.CompareTag("Enemy"))
        {
            other.gameObject.GetComponent<EnemyManager>().TakeDamage(1);
            DestroyBullet();
        }
    }


    private void OnCollisionEnter(Collision other)
     {
         Debug.Log("Collider");
         if (!other.collider.CompareTag("Bullet") && !other.collider.CompareTag("Player"))
         {
             Debug.Log("Collided with "  + other.gameObject.name);
             DestroyBullet();
         }
     }
}
