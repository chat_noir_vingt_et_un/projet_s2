﻿using System;
using UnityEngine;

public class SpellManager : MonoBehaviour
{
    [SerializeField] private GameObject[] SpellIcons;

    public int currentSpell;

    private void Start()
    {
        SpellIcons[0] = GameObject.Find("Fire");
        SpellIcons[1] = GameObject.Find("Water");
        SpellIcons[2] = GameObject.Find("Earth");
        SpellIcons[3] = GameObject.Find("Wind");
        
        foreach (var icon in SpellIcons)
        {
            icon.SetActive(false);
        }

        SpellIcons[GetComponent<PlayerManager>().CurrentSpell].SetActive(true);
    }

    private void Update()
    {
        int neededSpell = GetComponent<PlayerManager>().CurrentSpell;
        
        if (currentSpell != neededSpell)
        {
            SpellIcons[currentSpell].SetActive(false);
            SpellIcons[neededSpell].SetActive(true);
            currentSpell = neededSpell;
        }
    }
}
