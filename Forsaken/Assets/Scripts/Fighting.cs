﻿using UnityEngine;


public class Fighting : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public float damage;
    [SerializeField] public Camera cam;
    [SerializeField] public float coolDown;
    [SerializeField] public float attackTime; // in the future, these will belong to a weapon class and will be accessed through weapon component.
    private Animator anim;
    void Start()
    {
        damage = 1f;
        coolDown = 2f; // arbitrary, later will vary from weapon to weapon maybe
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    
    void Attac()
    {
        if (Input.GetMouseButtonDown(0) && attackTime >= coolDown)// get left mouse click and triggers if player is able to attack
        {
            attackTime = 0f;
            anim.SetTrigger("Slash");
            Ray r = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;// output a raycast
            if (Physics.Raycast(r, out hit, 7f))
            {
                if (hit.collider.CompareTag("Enemy"))// if hits enemy give damage to enemy
                {
                    enemy e = hit.collider.GetComponent<enemy>();
                    e.TakeDamage(damage);
                }
            }
        }
    }

    private void Update()
    {
        attackTime += Time.deltaTime; // how cooldown works, add time, when attacktime greater than 2 seconds, can attack again.
    }

    private void FixedUpdate()
    {
        Attac();
    }
}
