﻿using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] Sounds;
    
//    void Awake()
//    {
//        foreach (var s in Sounds)
//        {
//            s.source = GetComponent<AudioSource>();
//            s.source.clip = s.clip;
//            s.source.volume = s.volume;
//            s.source.pitch = s.pitch;
//            s.source.loop = s.loop;
//        }
//    }
    // Update is called once per frame
    public void Play(string name)
    {
        Sound s = Array.Find(Sounds, sound => sound.name == name);
        s.source = GetComponent<AudioSource>();
        s.source.clip = s.clip;
        s.source.volume = s.volume;
        s.source.pitch = s.pitch;
        s.source.loop = s.loop;
        if (s is null)
        {
            return;
        }
        //print("found");
        //print(s.source);
        s.source.Play();
    }
}    
