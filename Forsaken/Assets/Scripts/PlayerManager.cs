﻿using Photon.Pun;
using Tweleyes.Forsaken;
using UnityEngine;

public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
{

    #region public Fields

    [Tooltip("The max Health that out player can have")]
    public float maxHealth;
    
    [Tooltip("The current Health of our player")]
    public int Health = 3;

    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    private Animator anim;

    [Tooltip("The local player's camera")] [SerializeField]
    public  GameObject Camera;

    public float mana;

    
    public float cost;
    [Tooltip("The Player's Bulet   GameObject Prefab")]
    [SerializeField]
    public GameObject[] BulletPrefab;

    public int CurrentSpell;
    private float SpellCD = 0.5f;
    private float SpellChangeCD = 0.1f;

    private bool isShooting;

    public GameObject[] healthBar;
    public ManaBar b;

    #endregion
    #region Public Methods
    
    public void TakeDamage()
    {
        if (Health <= 0) return;
        healthBar[(int)Health - 1].SetActive(false);
        Health -= 1;
        Debug.Log(Health);
    }

    public void Shoot()
    {
        anim.SetTrigger("Cast Spell");
        PhotonNetwork.Instantiate("Spells/" + BulletPrefab[CurrentSpell].name, Camera.transform.position + Camera.transform.forward, Camera.transform.rotation);
        if (CurrentSpell == 3 && Health < maxHealth)
        {
            Health += 1;
            healthBar[Health - 1].SetActive(true);
        }
        
    }

    #endregion

    #region Monobehavior Callbacks

    public void Awake()
    {
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        if (photonView.IsMine)
        {
            PlayerManager.LocalPlayerInstance = this.gameObject;
        }

    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        b = FindObjectOfType<ManaBar>();
        mana = b.mana;
        cost = b.cost;
        healthBar[0] = GameObject.Find("1");
        healthBar[1] = GameObject.Find("2");
        healthBar[2] = GameObject.Find("3");
    }

    public void Update()
    {
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
        {
            return;
        }
        
        Animate();
        SpellChangeCD += Time.deltaTime;
        mana = b.mana;
        cost = b.cost;
        if (Health <= 0)
        {
            GameManager.Instance.LeaveRoom();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            TakeDamage();
        }
        if (SpellChangeCD > 0.2f)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                if ( CurrentSpell < 3)
                {
                    CurrentSpell += 1;
                }
                else
                {
                    CurrentSpell = 0;
                }
            }
            
            if (Input.GetAxis("Mouse ScrollWheel") < 0f )
            {

                if ( CurrentSpell >= 1)
                {
                    CurrentSpell -= 1;
                }
                else
                {
                    CurrentSpell = 3;
                }
            }
        }
        
        //checks if we can shoot
        if (Input.GetMouseButtonDown(1) && cost <= mana)
        {
            if (!Camera.GetComponentInParent<MoveCamera>().paused)
            {
                Shoot();
            }
        }
    }
    
    #endregion

    public void Animate()
    {
        anim.SetFloat("Sideway", Input.GetAxis("Horizontal"));
        anim.SetFloat("Forward", Input.GetAxis("Vertical"));
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(Health);
        }
        else
        {
            // Network player, receive data
            Health = (int)stream.ReceiveNext();
        }
    }
    
}
