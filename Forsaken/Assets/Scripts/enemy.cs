﻿using UnityEngine;

public class enemy : MonoBehaviour
{
    [SerializeField] public float helth;
    [SerializeField] public GameObject obj;

    private void Start()
    {
        helth = 3f;
    }

    public void TakeDamage(float damage)
    {
        //print("OUCH");
        helth -= damage;
        if (helth <= 0)
        {
            //print("ARGH");
            Destroy(obj);
        }
    }
}
