﻿ using Photon.Pun;
 using UnityEngine;

public class MouseLook : MonoBehaviourPun
{

    public static float MouseSensi = 0.03f;
    private float Xrotation = 0f;    
    public Transform PlayerBody;
    public GameObject Camera;
    
    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected)
        {
            Camera.SetActive(false);
        }
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected)
        {
            return;
        }

        if (Cursor.lockState == CursorLockMode.Locked)
        {
            //makes sure the cursor is invisible when playing
            Cursor.visible = false;

            //gets the mouse input
            float mouseX = Input.GetAxis("Mouse X") * MouseSensi / Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * MouseSensi / Time.deltaTime;

            //processes the y input, limits it with Clamp()
            Xrotation -= mouseY;
            Xrotation = Mathf.Clamp(Xrotation, -90, 90);
            //rotates the player body on the z axis (will rotate the camera as it is a child of the body)
            PlayerBody.Rotate(Vector3.up * mouseX);
            //rotates the camera on the x axis
            Camera.transform.localRotation = Quaternion.Euler(Xrotation,0f,0f);
        }
        

    }
}
