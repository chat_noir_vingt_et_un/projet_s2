﻿using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.AI;
using Random = System.Random;

public class EnemyManager : MonoBehaviourPunCallbacks
{
    [Tooltip("the health of the enemy, when it falls below 0 the enemy dies")]
    public int Health;

    public float attackrange;
    public float timeToAttack;

    public Animator anim;

    public NavMeshAgent agent;
    
    Random random = new Random();
    
    /// <summary>
    /// The list of players in range of an enemy, all such player are tracked  
    /// </summary>
    private List<Collider> InRangePlayers = new List<Collider>();
    
    private float cooldown = 0f;
    private bool damage = false;

    private void Start()
    {
	timeToAttack = 6f; //Needs to be changed to fit the enemies animation.
    }

    void Update()
    {
        cooldown += Time.deltaTime;
        //Bounds the AI control to the master client
        if (PhotonNetwork.IsMasterClient == false && PhotonNetwork.IsConnected)
        { 
            return;
        }
        //-------------- Actual AI --------------
        // if the Enemy has someone in viewrange, it sets a path to the target's location
        
        bool Istracking = InRangePlayers.Count != 0; ;
        
        if (anim.GetBool("Alive") && Istracking)
        {
            Vector3 target = InRangePlayers[0].gameObject.transform.position;

            if ( CheckVisibility(target) && anim.GetCurrentAnimatorStateInfo(0).IsName("Golem|IDLE"))
            {
                damage = false;
                agent.SetDestination(target);
                target.y = 1.0f;
                transform.LookAt(target);
            }
            
            if (cooldown > timeToAttack)
            {
                if (CheckDistance(target) <= 3)
                {
                    cooldown = 0;
                    float i = 0;
                    if (random.Next(0, 2) == 0)
                    {
                        anim.SetTrigger("One hand Attack");
                        while (i < 2.17f)
                        {
                            i += Time.deltaTime;
                        }

                        if (i > 1.8 && !damage)
                        {
                            damage = true;
                            InRangePlayers[0].GetComponent<PlayerManager>().TakeDamage();
                            Debug.Log($"{gameObject.name} damaged player");
                            //InRangePlayers[0].GetComponent<PlayerManager>().Health -= 1;
                        }
                    
                    }
                    else
                    {
                        anim.SetTrigger("Two hands Attack");
                        while (i < 2.17f)
                        {
                            i += Time.deltaTime;
                        }
                        if (i > 1.8 && !damage)
                        {
                            damage = true;
                            InRangePlayers[0].GetComponent<PlayerManager>().Health -= 1;
                        }
                    }
                }
            }
        }
    }

    public void Attack()
    {
        Debug.Log("Enemy attacked");
    }
    
    /// <summary>
    /// This function adds a new player to the list of tracked player, should no be called by this gameobject, it will be called by it's child "tracker"
    /// </summary>
    /// <param name="player">A player collider, will be given by a OnColliderEnter</param>
    public void AddTracked(Collider player)
    {
        if (!InRangePlayers.Contains(player))
        {
            Debug.LogFormat("Enemy Manager : player is tracked");
            InRangePlayers.Add(player);
        }
    }

    public void RemoveTracked(Collider player)
    {
        Debug.LogFormat("Enemy Manager : player is lost");
        InRangePlayers.RemoveAt(InRangePlayers.Count - 1);
    }
    
    public void TakeDamage(int damageDone)
    {
        Debug.Log("Enemy took " + damageDone + " damage");
        Health -= damageDone;
        if (Health <= 0)
        {
            anim.SetBool("Alive", false);
        }
    }

    public bool CheckVisibility(Vector3 pos)
    {
        RaycastHit hit;
        Physics.Raycast(transform.position,pos - transform.position , out hit);
        if (hit.collider == null) return false;
        if (hit.collider.gameObject.layer == 9)
        {
            return true;
        }

        return false;
    }

    public float CheckDistance(Vector3 target)
    {
        target.y = 0.0f;
        Vector3 pos = transform.position;
        pos.y = 0.0f;
        return Vector3.Distance(target, pos);
    }


}
