﻿using UnityEngine;


namespace Tweleyes.Forsaken
{
    public class Tracker : MonoBehaviour
    {
        [Tooltip("A reference to the parent object")] [SerializeField]
        private GameObject parent;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                //Debug.Log("Tracker : player is tracked");
                parent.GetComponent<EnemyManager>().AddTracked(other);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                //Debug.Log("Tracker : player is lost");
                parent.GetComponent<EnemyManager>().RemoveTracked(other);
            }
        }
    }

}