﻿using UnityEngine;
using UnityEngine.UI;


public class Pickup : MonoBehaviour
{
    [SerializeField] public Text prompt;
    [SerializeField] public GameObject hand;
    [SerializeField] public Fighting stats;
    private void Start()
    {
        //get weapon stats
        //disables active weapon.
        stats = GetComponent<Fighting>();
        hand.SetActive(false);
    }

    private void OnTriggerStay(Collider obj) // if in range of obj collider
    {
        if (obj.gameObject.CompareTag("Object"))
        {
            prompt.gameObject.SetActive(true);// enables prompt, pickup text and triggers the pickup method
            Pick(obj);
        }
    }

    private void OnTriggerExit(Collider other)//disables prompt when we leave the zone
    {
        prompt.gameObject.SetActive(false);
    }

    public void Pick(Collider obj)
    {
        if (Input.GetKeyDown(KeyCode.E))//when e pressed, enables the weapon in hand, disables prompt, destroys weapon on ground assign the weapon's damage
        {
            hand.SetActive(true);
            stats.damage = 2f;
            prompt.gameObject.SetActive(false);
        }
    }
}
