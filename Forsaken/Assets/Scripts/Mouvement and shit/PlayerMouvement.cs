﻿using Photon.Pun;
using UnityEngine;

public class PlayerMouvement : MonoBehaviourPun
{

    public CharacterController Controller;

    public Transform groundCheck;
    public Transform roofCheck;
    
    public LayerMask groundmask;

    public Vector3 dimensions;
    
    public float speed = 12f;
    public float gravity = -9.81f;
    public float JumpHeight = 3;

    private bool isGrounded;
    private bool isRoofed;
    
    private Vector3 velocity;

    void Update()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected)
        {
            return;
        }
        
        isGrounded = Physics.CheckBox(groundCheck.position,dimensions,Quaternion.identity,groundmask);
        isRoofed = Physics.CheckBox(roofCheck.position,dimensions,Quaternion.identity,groundmask);

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        Controller.Move(move * (Time.deltaTime * speed));

        if (isGrounded) // sets vertical velocity if grounded
        {
            velocity.y = -2;
        }
        if (isRoofed) // sets vertical velocity if grounded
        {
            velocity.y = -1;
        }

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(-2f * JumpHeight * gravity);
        }
        
        if (!isGrounded) // decreases vetical velocity if not on the ground
        {
            velocity.y += gravity * Time.deltaTime;
        }
        
        Controller.Move(velocity * Time.deltaTime);
    }
}
