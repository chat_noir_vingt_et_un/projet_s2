using System;
using Photon.Pun;
using UnityEngine;

public class MoveCamera : MonoBehaviourPunCallbacks {

    public Transform cameraPosParent;
    public GameObject camera;
    public bool paused = false;

    private void Start()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected)
        {
            camera.SetActive(false);
        }
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() 
    {
        //Debug.Log(photonView.IsMine == false && PhotonNetwork.IsConnected);
        if (photonView.IsMine == false && PhotonNetwork.IsConnected)
        {
            return;
        }
        // frees or locks the cursor when pressing escape
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }

        camera.transform.position = cameraPosParent.position - Vector3.up * 0.3f ;
    }
}
