﻿using Photon.Pun;
using UnityEngine;

public class MovementRework : MonoBehaviourPun
{
    [SerializeField] private Rigidbody body;
    [SerializeField] private float walkSpeed = 1500f;
    [SerializeField] private float sprintSpeed = 2000f;
    [SerializeField] private float currentSpeed;
    [SerializeField] public float jumpForce = 1000f;

    [SerializeField] private bool onSlope;
    private bool IsJumping;

    // Start is called before the first frame update
    private void Start()
    {
        onSlope = false;
        body = GetComponent<Rigidbody>();
    }
    
    // Update is called once per frame
    private void Update()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected)
        {
            return;
        }
        Move();
        Jump();
    }

    private void FixedUpdate()
    {
        IsGrounded();
        Gravity();

        body.isKinematic = false;

        if (onSlope)
            if (!GetInput())
                body.isKinematic = true;
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        body.freezeRotation = true;
        if (collision.collider.CompareTag("Slope")) onSlope = true;
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.collider.CompareTag("Slope")) onSlope = false;
    }
    
    private void Move()
    {
        Sprint();
        var xA = Input.GetAxisRaw("Horizontal");
        var zA = Input.GetAxisRaw("Vertical");

        var direction = (Vector3.forward * zA + Vector3.right * xA).normalized;

        if (xA > 0f || xA < 0f || zA > 0f || zA < 0f || !IsGrounded())
            body.drag = 2f;
        else if (IsGrounded()) body.drag = 5f;

        var input = (direction * body.mass + body.velocity.y * Vector3.up) * currentSpeed;


        var movement = body.position + body.transform.TransformDirection(input);
        body.AddForce(movement * Time.deltaTime, ForceMode.Force);
    }

    private bool IsGrounded()
    {
        //float est arbitraire, faudra set a la distance player sol
        var position = transform.position;
        
        Debug.DrawRay(position, Vector3.down * 3.4f, Color.blue, 5f);
        return Physics.Raycast(position, Vector3.down, 3.4f);
    }

    private void Gravity()
    {
        if (IsJumping)
        {
            body.AddForce(jumpForce * Vector3.up, ForceMode.Force);

        }
        else
        {
            body.AddForce(200 * Vector3.down, ForceMode.Force);

        }
    }
    private void Jump()
    {
        if (IsGrounded())
        {
            if (Input.GetKeyDown("space"))
                IsJumping = true;
            //body.AddForce(body.mass * jumpSpeed * Time.deltaTime * Vector3.up, ForceMode.Impulse);
            //body.AddForce(jumpForce * Time.deltaTime * Vector3.up, ForceMode.VelocityChange);
        }
        else
        {
            IsJumping = false;
            //body.AddForce(jumpSpeed * Vector3.down);
            //body.AddForce(30 * Time.deltaTime * Vector3.down, ForceMode.Force);
        }
    }

    private bool GetInput()
    {
        return Input.GetAxisRaw("Horizontal") < 0f || Input.GetAxisRaw("Horizontal") > 0f ||
               Input.GetAxisRaw("Vertical") > 0f || Input.GetAxisRaw("Vertical") < 0f;
    }

    private void Sprint()
    {
        if (Input.GetKey(KeyCode.LeftShift))
            currentSpeed = sprintSpeed;
        else
            currentSpeed = walkSpeed;
    }

}