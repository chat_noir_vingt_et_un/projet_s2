﻿using UnityEngine;

    [RequireComponent(typeof(Rigidbody))]
    public class Mouvent2 : MonoBehaviour
    {
        #region PUBLIC FIELDS

        [Header("Walk / Run Setting")] 
        public float walkSpeed;
        public float runSpeed;

        [Header("Jump Settings")] 
        public float playerJumpForce;
        public ForceMode appliedForceMode;

        [Header("Jumping State")] 
        public bool playerIsJumping;

        [Header("Current Player Speed")] 
        public float currentSpeed;

        #endregion

        #region PRIVATE FIELDS

        private float _xAxis;
        private float _zAxis;
        private Rigidbody _rb;
        private RaycastHit _hit;
        private Vector3 _groundLocation;
        private bool _isCapslockPressedDown;

        #endregion

        #region MONODEVELOP ROUTINES

        private void Start()
        {
            #region Initializing Components

            _rb = GetComponent<Rigidbody>();

            #endregion
        }

        private void Update()
        {
            #region Controller Input

            _xAxis = Input.GetAxis("Horizontal");
            _zAxis = Input.GetAxis("Vertical");

            #endregion

            #region Adjust Player Speed

            currentSpeed = _isCapslockPressedDown ? runSpeed : walkSpeed;

            #endregion

            #region Player Jump Status

            playerIsJumping = Input.GetKeyDown(KeyCode.Space);

            #endregion
            
            //raycast debug
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 10f, Color.blue);
            
            //raycast to see if we can jump aka we can't jump if we are in the air
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out _hit,
                Mathf.Infinity))
            {
               // if (String.Compare(_hit.collider.tag, "Wall", StringComparison.Ordinal) == 0)
               // {
                    _groundLocation = _hit.point;
               // }

                var distanceFromPlayerToGround = Vector3.Distance(transform.position, _groundLocation);
                Debug.Log(distanceFromPlayerToGround);
                
                // 3.35 is about the character height, if the ray is longer than 3.35 than we cannot jump
                //the height can be found by debugging distanceFromplayerToground     
                if (distanceFromPlayerToGround > 3.5f)
                    playerIsJumping = false;
                
                //makes the player jump
                if(playerIsJumping)
                    Jump(playerJumpForce, appliedForceMode);
            }

        }

        private void FixedUpdate()
        {
            // moves the player
            _rb.MovePosition(transform.position + Time.deltaTime * currentSpeed * 
                             transform.TransformDirection(_xAxis, 0f, _zAxis));
        }

        #endregion
        
        //Jump function, adds force to the rigidbody and makes de player jump
        private void Jump(float jumpForce, ForceMode forceMode)
        {
            _rb.AddForce(jumpForce * _rb.mass * Time.deltaTime * Vector3.up, forceMode);
        }
    }