﻿using UnityEngine.Serialization;

namespace Resources.Objects.Weapons
{
    public class WeaponClass : Item
    {
        public int damage;
        public WeaponClass(string name, string description, int damage)
        {
            Name = name;
            Description = description;
            this.damage = damage;
        }
    }
}
