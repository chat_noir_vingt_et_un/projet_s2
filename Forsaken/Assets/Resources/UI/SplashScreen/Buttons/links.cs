﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class links : MonoBehaviour
{

    public void website() {
        Application.OpenURL("https://chat_noir_vingt_et_un.gitlab.io/projet_s2/");
    }

    public void steam()
    {
        Application.OpenURL("https://www.youtube.com/channel/UC_x2tDHoMRxHmvW7pmvr2sQ?view_as=subscriber");
    }

    [SerializeField]
    public AudioMixer audioMixer;

    public void SetVolume (float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }
}
