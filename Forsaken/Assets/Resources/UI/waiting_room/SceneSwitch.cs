﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{

    public void ToWaitingRoom()
    {
        SceneManager.LoadScene ("WaitingRoom");
    }

    public void ToNewLauncher()
    {
        SceneManager.LoadScene("newLauncher");
    }
}
