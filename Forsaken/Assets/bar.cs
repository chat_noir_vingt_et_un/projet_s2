﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bar : MonoBehaviour
{
    private Image i;
    private float cooldown;
    private circle c;
    // Start is called before the first frame update
    void Start()
    {
        cooldown = 0;
        c = GetComponentInParent<circle>();
        i = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        cooldown += Time.deltaTime;
        if (c.empty && c.i.fillAmount >= 1)
        {
            c.empty = false;
        }
        else if (i.fillAmount <= 0 && c.i.fillAmount <= 1)
        {
            c.empty = true;
        }    
        else
        {
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                cooldown = 0;
                i.fillAmount -= 0.25f;
            }
        }
        //print(c.empty);
        if (cooldown >= 2 && !c.empty)
        {
            i.fillAmount += Time.deltaTime / 5;
        }
    }
}
