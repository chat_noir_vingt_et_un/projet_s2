﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Map.WFC;
using System.IO;
using UnityEngine.Windows;
using File = System.IO.File;

public class TestScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // MapLoader.Load(File.ReadAllText("../map.txt"));
        // return;
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 60;

        //MasterCLient
        MapGen m;
        Dictionary<Tnode, Tstate> result = null;
        do
        {
            m = new MapGen();
            result = m.Generate(300);
        } while (result == null);
        
        string textout = MapGenIO.WriteDefinitionFile(result, m.WorldSize, m.CellSize);
        
        // tout le monde
        Debug.Log("Loading map");
        
        // part 2
        MapLoader.Load(textout);
        Debug.Log("Map loaded");
    }
}
