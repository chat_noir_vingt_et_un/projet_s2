﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.PlayerLoop;

public class Doors : MonoBehaviour
{
    private Vector3 center;
    private Vector3 RawCenter;
    private BoxCollider boxA;
    private BoxCollider boxB;
    private bool opened = false;

    public void CalcCenter()
    {
        float x = 0;
        float y = 0;
        float z = 0;
        
        Vector3[] a = transform.GetChild(0).GetComponent<MeshFilter>().mesh.vertices;
        foreach (var v in a)
        {
            x += v.x;
            z += v.z;
        }
        int c = a.Length;
        x /= c;
        z /= c;

        Vector3 p = new Vector3(x, y, z);
        RawCenter = p;
        p += new Vector3(0.18f, 0, 0);
        p = transform.rotation * p;
        p += transform.position;
        center = p;
    }

    private void Open(int angle)
    {
        transform.RotateAround(center, Vector3.up, angle);
    }

    public void Start()
    {
        CalcCenter();
        boxA = gameObject.AddComponent<BoxCollider>();
        boxB = gameObject.AddComponent<BoxCollider>();
        
        boxA.center = new Vector3(-0.5f, 0.5f, -1f);
        boxA.size = new Vector3(1, 1, 0.5f);
        
        boxB.center = new Vector3(-0.5f, 0.5f, 0f);
        boxB.size = new Vector3(1, 1, 0.5f);
        
        boxA.isTrigger = true;
        boxB.isTrigger = true;
        //gameObject.layer = 31;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (opened || !other.CompareTag("Player"))
        {
            return;
        }
        var position = other.transform.position;
        var rotation = transform.rotation;
        var position1 = transform.position;
        float distanceA = Vector3.Distance(a: rotation * boxA.center + position1, position);
        float distanceB = Vector3.Distance(rotation * boxB.center + position1, position);
        //Debug.Log(boxA.center);
        //Debug.Log(boxB.center);
        if (distanceA > distanceB)
        {
            Open(-90);
        }
        else
        {
            Open(90);
        }
        opened = true;
        boxA.enabled = false;
        boxB.enabled = false;
    }
}
