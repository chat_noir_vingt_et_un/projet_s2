﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class gen_floor : MonoBehaviour
{
    public int deltaTime = 0;
    private double timeTilNow = 0;
    private int i = 0;
    private int j = 0;
    private bool active = true;

    void Update()
    {
        if (active)
        {
            InstantiateObj(gameObject, @"models\dungeon\struct_floor_normal", new Vector3(i, 0, j), new Vector3());
            if (i == 21 && j == 20) active = false;
            if (j > 20)
            {
                j = 0;
                i = 0;
            }
            else if (i > 20)
            {
                i = 0;
                j++;
            }
            else
            {
                i++;
            }
        }
    }

    private static GameObject InstantiateObj(GameObject parent, string path, Vector3 pos, Vector3 rot)
    {
        Object o = Instantiate(UnityEngine.Resources.Load(path), new Vector3(0, 0, 0), Quaternion.identity);
        GameObject go = (GameObject) o;
        
        //GameObject child = go.transform.GetChild(0).gameObject;
        GameObject child = go;
        
        child.transform.position = pos - new Vector3(0.5f, 0,0.5f);
        child.transform.rotation = Quaternion.Euler(rot);
        child.transform.SetParent(parent.transform, false);
        child.name = path;
        //Destroy(o);
        return child;
    }
}
