﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Map.WFC;
using UnityEditor;
using UnityEditor.Rendering.HighDefinition;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Analytics;
using Object = UnityEngine.Object;

public class MapLoader : MonoBehaviour
{
    private static readonly int Emission = Shader.PropertyToID("_EMISSION");
    private static readonly int Color = Shader.PropertyToID("_Color");

    public static void Load(string text)
    {
        // variables
        string pathRoot = @"models\";
        string[] lines = text.Split(new string[] {"\n"}, StringSplitOptions.RemoveEmptyEntries);
        Vector3 WorldSize;
        Vector3 CellSize;

        // master game object for the map
        GameObject MasterParent = new GameObject();
        MasterParent.name = "The Map";
        
        // parent game object representing a room
        GameObject parent = null;
        
        // ground mesh
        List<Vector3> vertices = new List<Vector3>();
        List<int> tris = new List<int>();
        
        // golem (parent, pathRoot + name, pos, rot, scale)
        List<(GameObject, string, Vector3, Vector3, Vector3)> golemList = new List<(GameObject, string, Vector3, Vector3, Vector3)>();
        
        //parse first line for world information
        if (lines[0][0] != '=') throw new Exception("no map info header found");
        string[] worldInfo = lines[0].Split(new[] {"\t", " "}, StringSplitOptions.RemoveEmptyEntries);
        WorldSize = Utils.ParseV3(worldInfo[1]);
        CellSize = Utils.ParseV3(worldInfo[2]);
        
        // create all point for ground collider
        for (int i = 0; i < WorldSize.x + 1; i++)
            for (int j = 0; j < WorldSize.z + 1; j++)
                vertices.Add(new Vector3(i, 0, j));
            
        
        // state variable
        bool isFloor = false;
        int old_i = 0;
        int old_j = 0;
        int rotZ = 0;
        int size = (int)WorldSize.z + 1;
        GameObject go;
        
        HashSet<int> loaded = new HashSet<int>(); // hashes of the models loaded
        long modelCount = 0;

        foreach (var line in lines)
        {
            string[] elts = line.Split(new string[] {"\t", " "}, StringSplitOptions.RemoveEmptyEntries);
            
            if (elts[0][0] == '=')  continue;
            
            // if info about a room 2x2
            if (elts[0] == ">")
            {
                if (parent != null && isFloor) // create triangles
                {
                    AddTri(tris, old_i, old_j, size);
                    isFloor = false;
                }
                parent = new GameObject();
                rotZ = int.Parse(elts[3]);
                parent.transform.rotation = Quaternion.Euler(0, -rotZ, 0);
                parent.transform.localScale = Utils.ParseV3(elts[2]);
                Vector3 tmpPos = Utils.ParseV3(elts[1]);
                old_i = (int)tmpPos.x;
                old_j = (int)tmpPos.z;
                parent.transform.localPosition = new Vector3(tmpPos.x*CellSize.x, tmpPos.y*CellSize.y, tmpPos.z*CellSize.z);
                parent.name = elts[1];
                parent.transform.SetParent(MasterParent.transform, false);
                continue;
            }
            if (parent == null)
            {
                throw new Exception("parent not init");
            }
            
            // path pos scale Zrot scale collider
            string name = RmExt(elts[0]);
            Vector3 pos = Utils.ChgtConvention(Utils.ParseV3(elts[1]));
            Vector3 rot = (ParseVect3("0", elts[2], "0") * -1) + new Vector3(0, -90, 0);
            Vector3 scale = Utils.ChgtConvention(Utils.ParseV3(elts[3]));
            bool collider = elts[4] == "o"; // does object need collider

            /*
            if (name == @"golem/Golem")
            {
                golemList.Add((parent, pathRoot + name, pos, rot, scale));
                continue;
            }
            */
            
            // Instantiate Obj
            go = InstantiateObj(parent, pathRoot + name, pos, rot, scale);
            Utils.MoveToLayer(go.transform, LayerMask.NameToLayer("Ground"));


            if (name == @"dungeon\struct_floor_normal" ||
                name == @"modular_village_collection\Modular_Village\Stone_Floor_1" ||
                name == @"modular_village_collection\Modular_Village\Stone_Floor_2")
            {
                isFloor = true;
            }
            int modelHash = ModelHash(go);
            if (loaded.Contains(modelHash))
            {
                Destroy(go);
                modelCount++;
            }
            else
            {
                loaded.Add(modelHash);
                if (collider) AddCollider(go);
                modelCount++;
            }
            
            if (name == @"dungeon\prop_wall_big_door_wood")
            {
                go.AddComponent<Doors>();
                Utils.MoveToLayer(go.transform, LayerMask.NameToLayer("Doors"));
            }

            if (name == @"torch")
            {
                Utils.MoveToLayer(go.transform, LayerMask.NameToLayer("Torch"));
            }

            if (name == @"golem/Golem")
            {
                Utils.MoveToLayer(go.transform, LayerMask.NameToLayer("Default"));
            }
        }
        
        Debug.Log($"loaded count = {loaded.Count}");
        Debug.Log($"model count = {modelCount}");

        if (isFloor) AddTri(tris, old_i, old_j, size);
        Mesh MasterMesh = new Mesh();
        MasterMesh.vertices = vertices.ToArray();
        MasterMesh.triangles = tris.ToArray();
        
        // mesh placement
        go = new GameObject();
        go.name = "floor collider";
        go.transform.SetParent(MasterParent.transform, false);
        go.transform.localScale = CellSize;
        go.transform.position = new Vector3(-CellSize.x/2, 0, -CellSize.z/2);
        go.layer = LayerMask.NameToLayer("Ground");
        
        // collider
        MeshCollider meshCollider = go.AddComponent<MeshCollider>();
        //meshCollider.material = (PhysicMaterial) UnityEngine.Resources.Load(@"Materials\Slippy walls");
        meshCollider.sharedMesh = MasterMesh;

        MasterParent.transform.localScale = new Vector3(1, 1, 1);
        MasterParent.transform.position = new Vector3(0, 1, 0);
        MasterParent.layer = LayerMask.NameToLayer("Ground");
        
        // NavMesh
        NavMeshSurface navMesh = CreateNavMesh();
        navMesh.BuildNavMesh();

        /*
        foreach (var (p, path , pos, rot, scale) in golemList)
        {
            go = InstantiateObj(p, path, pos, rot, scale);
            Utils.MoveToLayer(go.transform, LayerMask.NameToLayer("Default"));
        }*/
    }

    private static NavMeshSurface CreateNavMesh()
    {
        var go = new GameObject();
        go.name = "navMesh";
        NavMeshSurface navMesh = go.AddComponent<NavMeshSurface>();
        navMesh.layerMask = LayerMask.GetMask("Ground");
        return navMesh;
    }

    private static int ModelHash(GameObject go)
    {
        bool meshFound = false;
        long vertCount = 0;
        float minX = float.PositiveInfinity;
        float minY = float.PositiveInfinity;
        float minZ = float.PositiveInfinity;
        float maxX = float.NegativeInfinity;
        float maxY = float.NegativeInfinity;
        float maxZ = float.NegativeInfinity;
        
        foreach (var mesh in go.GetComponentsInChildren<MeshFilter>())
        {
            meshFound = true;
            vertCount += mesh.mesh.vertexCount;
            foreach (var vert in mesh.mesh.vertices)
            {
                minX = Mathf.Min(vert.x, minX);
                minY = Mathf.Min(vert.y, minY);
                minZ = Mathf.Min(vert.z, minZ);
                maxX = Mathf.Max(vert.x, maxX);
                maxY = Mathf.Max(vert.y, maxY);
                maxZ = Mathf.Max(vert.z, maxZ);
            }
        }
        Func<Vector3, string> disp = (v) => $"{v.x:F2},{v.y:F2},{v.z:F2}";
        Func<Vector3, Vector3> trans = (v) => (go.transform.rotation * v) + go.transform.position;
        Func<Vector3, string> f = (v) => disp(trans(v));

        Vector3 Vmin = new Vector3(); // default to (0,0,0)
        Vector3 Vmax = new Vector3();
        
        if (meshFound)
        {
            Vmin = new Vector3(minX, minY, minZ);
            Vmax = new Vector3(maxX, maxY, maxZ);
        }

        return $"{f((Vmin + Vmax) / 2)} {vertCount}".GetHashCode();
    }

    private static void AddTri(List<int> tris, int i, int j, int size)
    {
        // Create triangle 1
        tris.Add( j    +  i    * size);
        tris.Add((j+1) +  i    * size);
        tris.Add( j    + (i+1) * size);
        // Create triangle 2
        tris.Add( j    + (i+1) * size);
        tris.Add((j+1) +  i    * size);
        tris.Add((j+1) + (i+1) * size);
    }

    private static GameObject InstantiateObj(GameObject parent, string path, Vector3 pos, Vector3 rot, Vector3 scale)
    {
        Object o = Instantiate(UnityEngine.Resources.Load(path), new Vector3(0, 0, 0), Quaternion.identity);
        GameObject go = (GameObject) o;
        GameObject child = go;
        
        child.transform.position = pos - new Vector3(0.5f, 0,0.5f);
        child.transform.localScale = scale;
        child.transform.rotation = Quaternion.Euler(rot);
        child.transform.SetParent(parent.transform, false);
        child.name = path;
        foreach (var r in go.GetComponentsInChildren<Renderer>())
        {
            for (int i=0; i < r.materials.Length; i++)
            {
                Material mat = r.materials[i];
                mat.SetFloat("_DoubleSidedEnable", 1f);
                mat.SetFloat("_DoubleSidedNormalMode", 2f);
                mat.SetFloat("_AlbedoAffectEmissive", 1f);
                mat.SetColor("_EmissiveColor", UnityEngine.Color.white/20);
                mat.name = "CustomMaterial";
            }
        }
        return child;
    }

    private static void AddCollider(GameObject go)
    {
        foreach (var mesh in go.GetComponentsInChildren<MeshFilter>())
        {
            MeshCollider meshCollider = mesh.transform.gameObject.AddComponent<MeshCollider>();
            meshCollider.material = (PhysicMaterial) UnityEngine.Resources.Load(@"Materials\Slippy walls");
            meshCollider.sharedMesh = mesh.mesh;
        }
    }
    
    private static string RmExt(string str)
    {
        //Debug.Log(str);
        return Path.ChangeExtension(str, null);
    }

    private static Vector3 ParseVect3(string i, string j, string k)
    {
        int x = 0;
        int y = 0;
        int z = 0;
        if (!(int.TryParse(i, out x) && int.TryParse(j, out y) && int.TryParse(k, out z)))
        {
            Debug.Log($"FAILED TO PARSE {i} {j} {k}");
        }
        return new Vector3(x, y, z);
    }
}