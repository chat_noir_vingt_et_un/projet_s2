﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Map.WFC
{
    public class Tnode
    {
        public int x;
        public int y;
        public int z;
        public Vector3 size;

        public Tnode(int x, int y, int z, Vector3 worldSize)
        {
            size = worldSize;
            Utils.Assert(y == 0);
            this.x = x;
            this.y = y;
            this.z = z;

        }
        public Tnode this[int key]
        {
            get => GetNeighbourByIndex(key);
        }

        public Vector3 Pos => new Vector3(x, y, z);

        public Tnode GetNeighbourByIndex(int i)
        {
            switch (i)
            {
                case 0:
                    return E;
                case 1:
                    return N;
                case 2:
                    return W;
                case 3:
                    return S;
                case 4:
                    return U;
                case 5:
                    return D;
                default:
                    throw new Exception($"index not defined {i}");
            }
        }

        public Tnode N => z+1 < size[2] ? new Tnode(x, y, z + 1, size) : null;
        public Tnode S => z-1 >= 0      ? new Tnode(x, y, z - 1, size) : null;
        public Tnode E => x+1 < size[0] ? new Tnode(x + 1, y, z, size) : null;
        public Tnode W => x-1 >= 0      ? new Tnode(x - 1, y, z, size) : null;
        public Tnode U => y+1 < size[1] ? new Tnode(x, y + 1, z, size) : null;
        public Tnode D => y-1 >= 0      ? new Tnode(x, y - 1, z, size) : null;

        public static bool operator ==(Tnode a, Tnode b)
        {
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            return a.x == b.x && a.y == b.y && a.z == b.z;
        }

        public static bool operator !=(Tnode a, Tnode b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return $"({x.ToString()},{y.ToString()},{z.ToString()})";
        }

        // Dictionary
        public class EqualityComparer : IEqualityComparer<Tnode>
        {

            public bool Equals(Tnode x, Tnode y)
            {
                return x == y;
            }

            public int GetHashCode(Tnode x)
            {
                return x.x + 10000 * x.y + 1000000 * x.z;
            }
        }
    }  
}