﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using Map.WFC;

public class Tcp
{
    private TcpClient _client = new TcpClient("127.0.0.1", 10000);
    private NetworkStream _stream = null;

    public Tcp()
    {
        _stream = _client.GetStream();
    }

    public void SendRaw(string s)
    {
        Byte[] data = System.Text.Encoding.ASCII.GetBytes(s);
        _stream.Write(data, 0, data.Length);
    }

    public void Step(Collapser m, string msg="")
    {
        SendRaw($"NEXT: {msg}\n");
        foreach (var v in m.valid)
        {
            Tnode node = v.Key;
            HashSet<Tstate> states = v.Value;
            SendRaw($"NODE: {node.x}, {node.z}, {node.y}\n");
            foreach (var state in states)
            {
                SendRaw($"STATE: {state.ShortString()}\n");
            }
        }
        SendRaw($"SAVE:\n");
    }

    public void Close()
    {
        _stream.Flush();
        _stream.Close();
        _client.Close();
    }
}
