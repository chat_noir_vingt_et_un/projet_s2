﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = System.Random;

namespace Map.WFC
{
// some utils
    static class Utils
    {
        public static void Assert(bool t)
        {
            if (!t) throw new AssertionFailed();
        }
        
        public static void AssertNotNull<T>(T t)
        {
            if (t == null) throw new AssertionFailed();
        }

        public static void AssertEQ(int result, int expected)
        {
            if (result != expected) throw new AssertionFailed($"got: {result} / expected: {expected}");
        }
        
        public static void AssertNotZero(Vector3 v)
        {
            if (v.x == 0 && v.y == 0 && v.z == 0) throw new AssertionFailed("vector is (0, 0, 0)");
        }

        public static Vector3 ParseV3(string s)
        {
            var v = new Vector3();
            string[] coordStrings = s.Trim(new[] {'(', ')'}).Split(new[] {',', ' '}, StringSplitOptions.RemoveEmptyEntries);
            if (!float.TryParse(coordStrings[0], NumberStyles.Any, CultureInfo.InvariantCulture, out v.x))
                throw new Exception($"Parsing model coords failed: {s} ({coordStrings[0]})");
            if (!float.TryParse(coordStrings[1], NumberStyles.Any, CultureInfo.InvariantCulture, out v.y)) // y is altitude
                throw new Exception($"Parsing model coords failed: {s} ({coordStrings[1]})");
            if (!float.TryParse(coordStrings[2], NumberStyles.Any, CultureInfo.InvariantCulture, out v.z))
                throw new Exception($"Parsing model coords failed: {s} ({coordStrings[2]})");
            return v;
        }

        public static Vector3 ChgtConvention(Vector3 v)
        {
            (v.y, v.z) = (v.z, v.y);
            return v;
        }

        public static string Repr(Vector3 v)
        {
            return $"({v.x.ToString("0.0", CultureInfo.InvariantCulture)},{v.y.ToString("0.0", CultureInfo.InvariantCulture)},{v.z.ToString("0.0", CultureInfo.InvariantCulture)})";
        }

        public static string Reverse(string s)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = s.Length - 1; i >= 0; i--)
            {
                sb.Append(s[i]);
            }
            return sb.ToString();
        }

        public static float ParseFloat(string s)
        {
            return float.Parse(s, NumberStyles.Any, CultureInfo.InvariantCulture);
        }

        public static void MoveToLayer(Transform root, int layer) {
            root.gameObject.layer = layer;
            foreach(Transform child in root)
                MoveToLayer(child, layer);
        }
    }

// Main class
    public class Collapser
    {
        public Dictionary<Tnode, HashSet<Tstate>> valid;
        private Dictionary<Tnode, HashSet<Tnode>> dirty;
        protected HashSet<Tstate> states;
        private int statesCount;
        private Tnode[] nodes;
        private Stack<StateSnapshot> oldValids;
        protected Tcp tcp = null;
        public Random rnd;
        private int seed;

        public static string KeyArrayToString(Tnode[] a)
        {
            StringBuilder s = new StringBuilder();
            foreach (var k in a)
            {
                s.Append($"{k},");
            }

            return s.ToString();
        }
        public static HashSet<Tnode> GetFromDictionary(Dictionary<Tnode, HashSet<Tnode>> dic, Tnode node)
        {
            try
            {
                //Debug.Log($"GetFromDictionary: {dic[node]}");
                return dic[node];
            }
            catch (Exception)
            {
                //Debug.Log($"GetFromDictionary({KeyArrayToString(dic.Keys.ToArray())}, {node.x}): nothing");
                return new HashSet<Tnode>(new Tnode.EqualityComparer());
            }
            if (!dic.TryGetValue(node, out var value))
                return new HashSet<Tnode>(new Tnode.EqualityComparer());
            Debug.Log($"GetFromDictionary: {value}");
            return value;
        }

        public struct StateSnapshot
        {
            public Dictionary<Tnode, HashSet<Tstate>> valid;
            public Tnode node;
            public Tstate state;

            public StateSnapshot(Dictionary<Tnode, HashSet<Tstate>> valid, Tnode node, Tstate state)
            {
                this.valid = new Dictionary<Tnode, HashSet<Tstate>>(new Tnode.EqualityComparer()); // copy
                foreach (var KV in valid)
                {
                    this.valid[KV.Key] = KV.Value;
                }

                this.node = node;
                this.state = state;
            }
        }

        protected void DebugValidDirty(string title="")
        {
            StringBuilder s = new StringBuilder();
            s.Append($"{title}\nVALID:\n");
            foreach (var v in valid)
            {
                s.Append($"{v.Key} - ");
                foreach (var h in v.Value)
                {
                    s.Append($"{h}\n");
                }
                s.Append('\n');
            }
            s.Append("\nDIRTY:\n");
            foreach (var v in dirty)
            {
                s.Append($"{v.Key} - ");
                foreach (var h in v.Value)
                {
                    s.Append($"{h}, ");
                }
                s.Append('\n');
            }
            Debug.Log(s.ToString());
        }

        public void CollapserInit(Tnode[] nodes, HashSet<Tstate> states, int seed=default)
        {
            //Debug.Log($"Random Seed : {seed}");
            if (seed == default) rnd = new Random();
            else rnd = new Random(seed);
            
            this.valid = new Dictionary<Tnode, HashSet<Tstate>>(new Tnode.EqualityComparer());
            this.dirty = new Dictionary<Tnode, HashSet<Tnode>>(new Tnode.EqualityComparer());
            this.states = states;
            statesCount = states.Count;
            this.nodes = nodes;
            this.oldValids = new Stack<StateSnapshot>();
            if (DEBUG.MAIN) tcp = new Tcp();
            
            if (DEBUG.INIT) tcp.Step(this, "Initial State before anything");
            // compute stuff
            foreach (var n in nodes)
            {
                HashSet<Tstate> r = Restrict(n);
                valid[n] = new HashSet<Tstate>(new Tstate.EqualityComparer());
                if (r is null)
                    valid[n].UnionWith(states);
                else
                {
                    valid[n].UnionWith(r);
                    if (r.Count == 0)
                    {
                        Debug.Log(n.x);
                        throw new InconsistencyError($"Too restrictive at node {n}");
                    }

                    TagDirty(n);
                }
            }
            if (DEBUG.INIT) tcp.Step(this, "After all restrict");
            Propagate();
            if (DEBUG.INIT) tcp.Step(this, "After First Propagate (end of Init func)");
        }

        protected void Step()
        {
            try
            {
                if (DEBUG.STEP) tcp.Step(this, $"Before Observe (in Step)");
                Observe();
                if (DEBUG.STEP) tcp.Step(this, $"After Observe (in Step)");
                Propagate();
                if (DEBUG.STEP) tcp.Step(this, $"After Propagate (in Step)");
            }
            catch (InconsistencyError)
            {
                if (DEBUG.STEP) tcp.Step(this, $"Before Rewind (in Step)");
                Rewind();
                if (DEBUG.STEP) tcp.Step(this, $"After Rewind (in Step)");
            }
        }

        private void Rewind()
        {
            int i = 0;
            while (true)
            {
                if (DEBUG.REWIND) tcp.Step(this, $"Start of loop iteration {i} (in Rewind)");
                if (oldValids.Count == 0)
                {
                    throw new InconsistencyError("Rewound too far");
                }

                StateSnapshot snap = oldValids.Pop();
                valid = snap.valid;
                dirty = new Dictionary<Tnode, HashSet<Tnode>>(new Tnode.EqualityComparer());
                if (DEBUG.REWIND) tcp.Step(this, $"State is restored (in Rewind)");
                valid[snap.node].ExceptWith(new List<Tstate> {snap.state});
                if (DEBUG.REWIND) tcp.Step(this, $"Removed failing state (in Rewind)");
                if (valid[snap.node].Count == 0)
                    throw new InconsistencyError($"No valid choices at node {snap.node}");
                TagDirty(snap.node);
                try
                {
                    Propagate();
                    return;
                }
                catch (InconsistencyError e)
                {
                    Console.WriteLine($"(passed) {e}");
                }

                i++;
            }
        }

        private void Observe(Tnode node = null, Tstate value = null)
        {
            if (node is null)
            {
                bool noOptions = true;
                foreach (var KV in valid)
                {
                    if (KV.Value.Count == 1) continue;
                    if (noOptions && KV.Value.Count > 1) noOptions = false;
                    if (node is null) node = KV.Key;
                    if (KV.Value.Count < valid[node].Count) // current node nb of value < best node nb od value
                    {
                        node = KV.Key;
                    }
                }
                if (noOptions) return;
                List<Tnode> possibilities = new List<Tnode>();
                foreach (var KV in valid)
                {
                    if (KV.Value.Count == valid[node].Count)
                    {
                        possibilities.Add(KV.Key);
                    }
                }

                node = possibilities[rnd.Next(possibilities.Count)];
            }

            if (node is null) // DEBUG
                throw new Exception("NOOOOO");
            
            if (value is null)
            {
                value = ChooseState(node);
            }
            
            Utils.Assert(states.Contains(value));
            if (!valid[node].Contains(value))
            {
                throw new InconsistencyError("Observe: chosen state not allowed");
            }
            
            // copy valid
            Dictionary<Tnode, HashSet<Tstate>> validcpy = new Dictionary<Tnode, HashSet<Tstate>>();
            foreach (var a in valid)
            {
                Tnode n = a.Key;
                HashSet<Tstate> s = a.Value;
                validcpy[n] = new HashSet<Tstate>(s, new Tstate.EqualityComparer());
            }
            
            if (DEBUG.OBSERVE) tcp.Step(this, $"Saving current state in oldValid (in Observe)");
            oldValids.Push(new StateSnapshot(validcpy, node, value));
            valid[node] = new HashSet<Tstate>(new Tstate.EqualityComparer());
            valid[node].Add(value);
            if (DEBUG.OBSERVE) tcp.Step(this, $"Choice made (in Observe)");
            TagDirty(node);
            if (DEBUG.OBSERVE) tcp.Step(this, $"Dirty tagged (in Observe)");
        }

        private Tstate ChooseState(Tnode node)
        {
            Tstate[] arr = valid[node].ToArray();
            return arr[rnd.Next(arr.Length)];
        }

        private void Propagate()
        {
            while (dirty.Count != 0)
            {
                if (DEBUG.PROPAGATE) tcp.Step(this, $"Start of loop dirty.Count = {dirty.Count} (in Propagate)");
                var newDirty = new Dictionary<Tnode, HashSet<Tnode>>(new Tnode.EqualityComparer());
                foreach (var a in dirty)
                {
                    Tnode node = a.Key;
                    HashSet<Tnode> nbs = a.Value;
                    
                    var validStatesForNode = valid[node];
                    var n = validStatesForNode.Count;

                    foreach (Tnode nb in nbs)
                    {
                        HashSet<Tstate> con = Conset(node, nb);
                        validStatesForNode.IntersectWith(con);
                    }

                    if (validStatesForNode.Count == 0)
                        throw new InconsistencyError($"No valid choices at node {node}");

                    if (validStatesForNode.Count != n)
                    {
                        valid[node] = validStatesForNode;
                        TagDirty(node, newDirty);
                    }
                    if (DEBUG.PROPAGATE) tcp.Step(this, $"Node {node.Pos} cleaned (in Propagate)");
                }
                
                if (DEBUG.PROPAGATE) tcp.Step(this, $"Before updating to newDirty (in Propagate)");
                dirty = newDirty;
            }
        }

        private void TagDirty(Tnode node, Dictionary<Tnode, HashSet<Tnode>> d = null)
        {
            if (d is null) d = dirty;
            foreach (var nb in Neighbours(node))
            {
                Utils.Assert(node != nb);
                //var s = d[nb];
                // opti
                
                if (valid.ContainsKey(nb) && valid[nb].Count == 1)
                {
                    continue;
                }
                // opti
                HashSet<Tnode> s = GetFromDictionary(d, nb);
                s.Add(node);
                d[nb] = s;
            }
        }

        private HashSet<Tstate> Conset(Tnode node, Tnode neighbourNode)
        {
            var _states = new HashSet<Tstate>(new Tstate.EqualityComparer());
            foreach (var s in valid[neighbourNode])
            {
                HashSet<Tstate> cons = Consistent(node, neighbourNode, s);
                _states.UnionWith(cons);
                if (_states.Count == statesCount) break;
            }
            return _states;
        }

        public bool Resolved()
        {
            foreach (var tile in valid)
            {
                if (tile.Value.Count > 1) return false;
            }

            Debug.Log("~ Wave Function Collapse : Success ~");
            return true;
        }

        internal virtual HashSet<Tstate> Consistent(Tnode node, Tnode neighbourNode, Tstate s) // to overload
        {
            // Get a set of valid states for node, given nb == s
            throw new NotImplementedException("Consistent has not been implemented");
        }

        internal virtual Tnode[] Neighbours(Tnode node) // to overload
        {
            // Get a list of nodes which can be directly affected by node
            throw new NotImplementedException("Neighbours has not been implemented");
        }

        internal virtual HashSet<Tstate> Restrict(Tnode node) // to overload
        {
            // Get the set of possible states for a node (or None for any)
            throw new NotImplementedException("Restrict has not been implemented");
        }
    }




// Exceptions
    public class InconsistencyError : Exception
    {
        public InconsistencyError(string message) : base(message)
        {
        }
    }

    public class AssertionFailed : Exception
    {
        public AssertionFailed(string message) : base(message)
        {
        }
        public AssertionFailed()
        {
        }
    }
}