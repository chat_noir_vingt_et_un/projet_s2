﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Map.WFC
{
    public class Tstate
    {
        public string name;
        public string[] iface = new string[6];
        public List<Model> models = new List<Model>();
        public Option options;
        private int fillingCount = 0;
        public int rotZ = 0;
        public Vector3 scale = new Vector3(1, 1, 1);
        public bool[] passingIface = new bool[6];

        public Tstate(string[] iface) // E, N, W, S, U, D
        {
            Utils.AssertEQ(iface.Length, 6);
            iface.CopyTo(this.iface, 0);
        }
        
        public Tstate(string E, string N, string W, string S, string U, string D) // E, N, W, S, U, D
        {
            iface[0] = E;
            iface[1] = N;
            iface[2] = W;
            iface[3] = S;
            iface[4] = U;
            iface[5] = D;
        }

        public Tstate(Tstate old)
        {
            for (int i = 0; i < 6; i++)
            {
                iface[i] = old.iface[i];
            }

            fillingCount = old.fillingCount;
            rotZ = old.rotZ;
            scale = old.scale;
            passingIface = old.passingIface;
            models = new List<Model>();
            foreach (var m in old.models)
            {
                models.Add(new Model(m));
            }
            options = old.options;
            name = old.name;
        }

        public Tstate(string name)
        {
            this.name = name;
            options = new Option(1, true);
        }
        
        public void AddOption(Option o)
        {
            options.AddOptions(o);
        }

        public void ChoseOption(Random rnd)
        {
            models.AddRange(options.Resolve(rnd).Item1);
        }

        public void AddInterface(string dir, string i, string passing)
        {
            int index;
            switch (dir)
            {
                case "E":
                    index = 0;
                    break;
                case "N":
                    index = 1;
                    break;
                case "W":
                    index = 2;
                    break;
                case "S":
                    index = 3;
                    break;
                case "U":
                    index = 4;
                    break;
                case "D":
                    index = 5;
                    break;
                default:
                    throw new Exception($"AddInterface: unknown direction {dir}");
            }
            iface[index] = i;
            passingIface[index] = passing == "-";
            fillingCount++;
        }

        public void CheckFull()
        {
            if (fillingCount != 6) throw new Exception($"state does not have 6 ifaces {ToString()}");
            //if (models.Count == 0) throw new Exception("state has no models");
        }

        public string N => iface[1];
        public string S => iface[3];
        public string E => iface[0];
        public string W => iface[2];
        public string U => iface[4];
        public string D => iface[5];

        public Tstate RotatedCopy()
        {
            var state =  new Tstate(new [] {iface[3], iface[0], iface[1], iface[2], iface[4], iface[5]});
            state.rotZ = (rotZ + 90) % 360;
            state.name = name;
            state.models = new List<Model>();
            foreach (var m in models)
            {
                state.models.Add(new Model(m));
            }
            state.options = options;
            state.passingIface = new []{passingIface[3], passingIface[0], passingIface[1], passingIface[2], passingIface[4], passingIface[5]};
            return state;
        }
        
        public Tstate Mirror()
        {
            var state = new Tstate(new [] {iface[2], iface[1], iface[0], iface[3], iface[4], iface[5]});
            state.scale.x = -state.scale.x;
            state.models = models;
            state.options = options;
            state.passingIface = passingIface;
            return state;
        }

        public Tstate[] Variants() // TODO : rotate copy of mirror
        {
            Tstate[] arr = new Tstate[4];
            arr[0] = RotatedCopy();
            arr[1] = arr[0].RotatedCopy();
            arr[2] = arr[1].RotatedCopy();
            arr[3] = arr[2].RotatedCopy();
            //arr[4] = Mirror();
            return arr;
        }

        public bool IsCompatible(int dir, Tstate neighbour, Dictionary<string, HashSet<string>> ifaceDic)
        {
            if (dir == 4) return iface[dir] == neighbour.iface[5];
            if (dir == 5) return iface[dir] == neighbour.iface[4];
            Utils.Assert(0 <= dir && dir < 4);

            string revIface = Utils.Reverse(iface[dir]);
            string nIface = neighbour.iface[(dir + 2) % 4];

            bool r = true;
            for (int i = 0; i < nIface.Length; i++)
            {
                if (ifaceDic.ContainsKey(revIface[i].ToString()))
                {
                    r = r && ifaceDic[revIface[i].ToString()].Contains(nIface[i].ToString());
                }
                else
                {
                    r = r && revIface[i] == nIface[i];
                }
            }

            return r;
        }

        public bool IsPassing(int i)
        {
            Utils.Assert(0 <= i && i < 6);
            return passingIface[i];
        }

        public override string ToString()
        {
            return $"{iface[0]}, {iface[1]}, {iface[2]}, {iface[3]}, {iface[4]}, {iface[5]}";
        }
        
        public string ShortString()
        {
            return $"{iface[3][2]}{iface[2][2]}{iface[1][2]}{iface[0][2]}";
        }
        
        public static bool operator ==(Tstate a, Tstate b)
        {
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            for (int i = 0; i < a.iface.Length; i++)
            {
                if (a.iface[i] != b.iface[i]) return false;
            }
            return true;
        }

        public static bool operator !=(Tstate a, Tstate b)
        {
            return !(a == b);
        }
        
        // Dictionary
        public class EqualityComparer : IEqualityComparer<Tstate>
        {

            public bool Equals(Tstate x, Tstate y)
            {
                if (x is null && y is null) return true;
                if (x is null || y is null) return false;
                if (x.iface.Length != y.iface.Length) return false; // this should never happen
                for (int i = 0; i < x.iface.Length; i++)
                {
                    if (x.iface[i] != y.iface[i]) return false;
                }
                return true;
            }

            public int GetHashCode(Tstate x)
            {
                return $"{x.iface[0]}{x.iface[1]}{x.iface[2]}{x.iface[3]}{x.iface[4]}{x.iface[5]}".GetHashCode();
            }
        }
    }
}