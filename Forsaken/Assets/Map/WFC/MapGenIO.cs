﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Text;

namespace Map.WFC
{
    public static class MapGenIO
    {
        private static string debugOutPath = "../out.txt";
        private static string mapOutPath = "../map.txt";

        public static void DebugWriteToFile(string s)
        {
            File.WriteAllText(debugOutPath, s);
        }
        
        public static void DebugWriteToFile(Dictionary<Tnode, HashSet<Tstate>> dictionary)
        {
            StringBuilder s = new StringBuilder();
            foreach (var p in dictionary)
            {
                s.Append($"{p.Key} - {p.Value.ToArray()[0]}\n");
            }

            DebugWriteToFile(s.ToString());
            Debug.Log(s.ToString());
        }
        
        public static string WriteDefinitionFile(Dictionary<Tnode, Tstate> dictionary, Vector3 WorldSize, Vector3 CellSize)
        {
            StringBuilder text = new StringBuilder();
            text.Append($"= {Utils.Repr(WorldSize)} {Utils.Repr(CellSize)}\n");
            //string pathSource = @"Assets\Map\models\";
            foreach (var KV in dictionary)
            {
                var node = KV.Key;
                var state = KV.Value;
                text.Append($"> ({node.x},{node.y},{node.z}) {Utils.Repr(state.scale)} {state.rotZ}\n");
                foreach (var model in state.models) // model_path (x, y, z) rot
                {
                    text.Append($"{model}\n");
                }
            }
            
            //File.WriteAllText(mapOutPath, text.ToString());
            return text.ToString();
        }

        public static int[] ParseState(string[] s)
        {
            int[] data = new int[4];
            string[] coordStrings = s[1].Trim(new[] {'(', ')'}).Split(',');
            for (int i = 0; i < 3; i++)
            {
                if (!int.TryParse(coordStrings[i], out data[i]))
                    throw new Exception($"Parsing model coords failed: {s[1]}");
            }
            if (!int.TryParse(s[2], out data[3]))
                throw new Exception($"Parsing model rot failed: {s[2]}");
            return data;
        }
    }
}