﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Map.WFC
{
    static public class DEBUG
    {
        public static bool MAIN = false;
        public static bool INIT = false;
        public static bool REWIND = false;
        public static bool STEP = false;
        public static bool OBSERVE = false;
        public static bool PROPAGATE = false;
    }
    
    public class MapGen : Collapser
    {
        public Vector3 WorldSize;
        public Vector3 CellSize;
        public Tstate outside;
        public Tstate above;
        public Tstate below;
        private Dictionary<string, HashSet<string>> ifaceDic;
        private Dictionary<string, List<string>> compatDoor;
        private Dictionary<string, List<string>> compatWall;
        private Dictionary<string, Tstate> stateDic;
        
        private int seed = default;
        public Dictionary<Tnode, Tstate> Generate(int maxTries=40, int seed=default)
        {
            this.seed = seed;

            ifaceDic = new Dictionary<string, HashSet<string>>();
            stateDic = new Dictionary<string, Tstate>();
            HashSet<Tstate> baseSet = BlockReader();

            List<Tnode> nodes = new List<Tnode>((int) (WorldSize[0]*WorldSize[1]*WorldSize[2]));
            for (int i = 0; i < WorldSize[0]; i++)
            {
                for (int j = 0; j < WorldSize[1]; j++)
                {
                    for (int k = 0; k < WorldSize[2]; k++)
                    {
                        nodes.Add(new Tnode(i, j, k, WorldSize));
                    }
                }
            }

            HashSet<Tstate> tmpStates = new HashSet<Tstate>(new Tstate.EqualityComparer());
            foreach (var state in baseSet)
            {
                tmpStates.UnionWith(state.Variants());
            }
            
            Debug.Log($"Generated {tmpStates.Count} states from source file");

            CollapserInit(nodes.ToArray(), tmpStates, this.seed);
            int tries = 0;
            while (!Resolved() && tries < maxTries)
            {
                Step();
                if (DEBUG.MAIN) tcp.Step(this, $"Step nb {tries}");
                tries++;
            }

            if (tries >= maxTries)
            {
                Debug.Log("too many tries");
                return null;
                Debug.Log("ERROR");
                throw new InconsistencyError("Map gen failed");
            }

            Debug.Log($"Map Generated in {tries}");

            if (DEBUG.MAIN) tcp.Close();
            
            var cleanMap = new Dictionary<Tnode, Tstate>(new Tnode.EqualityComparer());
            foreach (var KV in valid)
            {
                Utils.Assert(KV.Value.Count == 1);
                cleanMap[KV.Key] = new Tstate(KV.Value.ToArray()[0]);
            }
            
            var post = new PostProcessing(cleanMap, states, rnd, WorldSize, compatDoor, compatWall, ifaceDic, outside, stateDic);
            Debug.Log("Start PostProcessing");
            if (!post.Process())
            {
                Debug.Log("Map not solvable");
                return null;
            }
            Debug.Log($"Map solvable");
            
            Debug.Log("Choosing options");
            foreach (var v in cleanMap)
            {
                v.Value.ChoseOption(rnd);
            }
            Debug.Log("Returning map");
            return cleanMap;
        }

        internal override HashSet<Tstate> Consistent(Tnode node, Tnode neighbourNode, Tstate s)
        {
            if (node == neighbourNode)
            {
                throw new Exception("Self constraint");
            }
            
            var stateList = new HashSet<Tstate>(states, new Tstate.EqualityComparer());
            
            if (neighbourNode.x + 1 == node.x )
                stateList =  new HashSet<Tstate>(
                    valid[node].Where(x => x.IsCompatible(2, s, ifaceDic)),
                    new Tstate.EqualityComparer());
            
            if (neighbourNode.x - 1 == node.x )
                stateList = new HashSet<Tstate>(
                    valid[node].Where(x => x.IsCompatible(0, s, ifaceDic)),
                    new Tstate.EqualityComparer());
            
            if (neighbourNode.z + 1 == node.z )
                stateList = new HashSet<Tstate>(
                    valid[node].Where(x => x.IsCompatible(3, s, ifaceDic)),
                    new Tstate.EqualityComparer());
            
            if (neighbourNode.z - 1 == node.z )
                stateList = new HashSet<Tstate>(
                    valid[node].Where(x => x.IsCompatible(1, s, ifaceDic)),
                    new Tstate.EqualityComparer());

            return stateList;
        }
        
        internal override HashSet<Tstate> Restrict(Tnode node)
        {
            var forbiddenStates = new HashSet<Tstate>(new Tstate.EqualityComparer());
            var allowedStates = new HashSet<Tstate>(states, new Tstate.EqualityComparer());
            for (int i = 0; i < 4; i++)
                if (node[i] == null) // test if out of bounds
                    foreach (var s in states)
                        if (!s.IsCompatible(i, outside, ifaceDic))
                            forbiddenStates.Add(s);

            if (node.Pos == new Vector3(2, 0, 0)) // ghetto door for temple
            {
                var r = new HashSet<Tstate>(new Tstate.EqualityComparer());
                Tstate bas = stateDic["corridor_side_door_ext"];
                Tstate[] states = bas.Variants();
                r.Add(states[2]);
                return r;
            }
            
            if (node.Pos == new Vector3(WorldSize.x - 3, 0, WorldSize.z - 1)) // ghetto door for exit
            {
                var r = new HashSet<Tstate>(new Tstate.EqualityComparer());
                Tstate bas = stateDic["corridor_side_door_ext"];
                Tstate[] states = bas.Variants();
                r.Add(states[0]);
                return r;
            }

            if (forbiddenStates.Count > 0)
            {
                allowedStates.ExceptWith(forbiddenStates);
                return allowedStates;
            }
            return null;
        }
        

        internal override Tnode[] Neighbours(Tnode node)
        {
            List<Tnode> neighbours = new List<Tnode>();
            if (node.x != 0)
                neighbours.Add(new Tnode(node.x - 1, node.y, node.z, WorldSize));

            if (node.x != (int)WorldSize[0] - 1)
                neighbours.Add(new Tnode(node.x + 1, node.y, node.z, WorldSize));
            
            /*
            if (node.y != 0 && node.y != height - 1)
            {
                neighbours.Add(new Tnode(node.x, node.y + 1, node.z));
                neighbours.Add(new Tnode(node.x, node.y - 1, node.z));
            }
            */

            if (node.z != 0)
                neighbours.Add(new Tnode(node.x, node.y, node.z - 1, WorldSize));

            if (node.z != (int)WorldSize[2] - 1)
                neighbours.Add(new Tnode(node.x, node.y, node.z + 1, WorldSize));
    
            
            if (neighbours.Count == 0)
            {
                Debug.Log(node);
            }
            Utils.Assert(neighbours.Count != 0);
            return neighbours.ToArray();
        }

        private HashSet<Tstate> BlockReader()
        {
            string data = UnityEngine.Resources.Load<TextAsset>("MapData/blocks").text;
            Debug.Log("Reading Map Settings");
            
            HashSet<Tstate> baseSet = new HashSet<Tstate>(new Tstate.EqualityComparer());
            
            compatDoor = new Dictionary<string, List<string>>();
            compatWall = new Dictionary<string, List<string>>();

            // state
            string state = null;
            Tstate currTstate = null;
            
            // Options variable
            Stack<Option> opt_stack = new Stack<Option>();
            opt_stack.Push(new Option(1, false));
            
            foreach (var line in data.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries))
            {
                string lineTrimed = line.Trim();
                //lineTrimed = lineTrimed.TrimStart(' ', '\t');
                
                if (lineTrimed == "") continue;
                if (lineTrimed[0] == '#') continue;

                string[] words = lineTrimed.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
                //Debug.Log(string.Join(", ", words));
                if (words[0][0] == '=')
                {
                    if (words[1] == "BLOCK")
                    {
                        state = "BLOCK";
                        if (currTstate != null)
                        {
                            currTstate.CheckFull();
                            currTstate.AddOption(opt_stack.Pop());
                            Utils.AssertEQ(opt_stack.Count, 0);
                            baseSet.Add(currTstate);
                            stateDic[currTstate.name] = new Tstate(currTstate);
                        }
                        currTstate = new Tstate(words[3]);
                        opt_stack.Clear();
                        opt_stack.Push(new Option(1, false));
                    }
                    else if (words[1] == "PARAM")
                    {
                        state = "PARAM";
                    }
                    else if (words[1] == "IFACE")
                    {
                        state = "IFACE";
                    }
                    else if (words[1] == "EQUIV")
                    {
                        state = "EQUIV";
                    }
                    else
                    {
                        throw new Exception($"Parser: undefined state : {state}\n{string.Join(", ", words)}");
                    }
                    continue;
                }
                
                if (words[0][0] == '>')
                {
                    state = words[1];
                }
                else if (state == "INTERFACES")
                {
                    currTstate.AddInterface(words[0], words[1], words[2]);
                }
                else if (state == "MODELS")
                {
                    currTstate.models.Add(new Model(string.Join(" ", words)));
                }
                else if (state == "PARAM")
                {
                    if (words[0] == "outside") outside = new Tstate(words[1], words[1], words[1], words[1], words[1],words[1]);
                    if (words[0] == "above") above = new Tstate(words[1], words[1], words[1], words[1], words[1],words[1]);
                    if (words[0] == "below") below = new Tstate(words[1], words[1], words[1], words[1], words[1],words[1]);
                    if (words[0] == "iMax") WorldSize.x = int.Parse(words[1]);
                    if (words[0] == "jMax") WorldSize.z = int.Parse(words[1]);
                    if (words[0] == "kMax") WorldSize.y = int.Parse(words[1]);
                    if (words[0] == "WorldSize") WorldSize = Utils.ChgtConvention(Utils.ParseV3(words[1]));
                    if (words[0] == "CellSize") CellSize = Utils.ChgtConvention(Utils.ParseV3(words[1]));
                    if (words[0] == "seed") seed = int.Parse(words[1]);
                }
                else if (state == "IFACE")
                {
                    if (!ifaceDic.ContainsKey(words[0]))
                        ifaceDic.Add(words[0], new HashSet<string>());
                    
                    if (!ifaceDic.ContainsKey(words[1]))
                        ifaceDic.Add(words[1], new HashSet<string>());
                    
                    ifaceDic[words[0]].Add(words[1]);
                    ifaceDic[words[1]].Add(words[0]);
                }
                else if (state == "OPTIONS")
                {
                    if (words[0] == "GRP")
                    {
                        bool BR = words.Length == 3 && words[2] == "BREAK";
                        opt_stack.Push(new Option(Utils.ParseFloat(words[1]), BR));
                    }
                    else if (words[0] == "END")
                    {
                        var option = opt_stack.Pop();
                        opt_stack.Peek().AddOptions(option);
                    }
                    else
                    {
                        opt_stack.Peek().AddModel(new Model(string.Join(" ", words)));
                    }
                }
                else if (state == "EQUIV")
                {
                    // wall --- door
                    if (!compatDoor.ContainsKey(words[0])) compatDoor[words[0]] = new List<string>();
                    if (!compatWall.ContainsKey(words[1])) compatWall[words[1]] = new List<string>();
                    compatDoor[words[0]].Add(words[1]);
                    compatWall[words[1]].Add(words[0]);
                }
                else
                {
                    throw new Exception($"Parser: undefined state : {state}\n{string.Join(", ", words)}");
                }
            }
            
            if (currTstate != null)
            {
                currTstate.CheckFull();
                currTstate.AddOption(opt_stack.Pop());
                Utils.AssertEQ(opt_stack.Count, 0);
                baseSet.Add(currTstate);
                stateDic[currTstate.name] = currTstate;
            }
            
            Utils.AssertNotNull(outside);
            Utils.AssertNotNull(above);
            Utils.AssertNotNull(below);
            Utils.AssertNotZero(WorldSize);
            Utils.AssertNotZero(CellSize);

            if (DEBUG.MAIN)
            {
                var tcp = new Tcp();
                tcp.SendRaw($"SET_SIZE: {WorldSize.x}, {WorldSize.z}, {WorldSize.y}");
                tcp.Close();
            }

            return baseSet;
        }
    }
}