using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;
using File = System.IO.File;
using Random = System.Random;

namespace Map.WFC
{
    public class PostProcessing
    {
        private Dictionary<Tnode, Tstate> map;
        private HashSet<Tstate> states;
        private Random rnd;
        private Vector3 worldSize;
        private Tnode start;
        private Tnode finish;
        private Dictionary<string, List<string>> compatDoor;
        private Dictionary<string, List<string>> compatWall;
        private Dictionary<string, HashSet<string>> ifaceDic;
        private Tstate outside;
        private Dictionary<string, Tstate> stateDic;

        public PostProcessing(Dictionary<Tnode, Tstate> cleanMap, HashSet<Tstate> states, Random rnd, Vector3 worldSize,
            Dictionary<string, List<string>> compatDoor, Dictionary<string, List<string>> compatWall,
            Dictionary<string, HashSet<string>> ifaceDic, Tstate outside, Dictionary<string, Tstate> stateDic)
        {
            map = cleanMap;
            this.states = states;
            this.rnd = rnd;
            this.worldSize = worldSize;
            start = new Tnode(2, 0, 0, worldSize);
            finish = new Tnode((int) worldSize.x - 3, 0, (int) worldSize.z - 1, worldSize);
            this.compatDoor = compatDoor;
            this.compatWall = compatWall;
            this.ifaceDic = ifaceDic;
            this.outside = outside;
            this.stateDic = stateDic;
        }

        public bool Process()
        {
            while (AddDoor())
            {
            }

            var doors = FindDoors();
            foreach (var (a, b) in doors)
            {
                if (IsToClose(a, b)) MakeWall(a, b);
            }

            RmInaccessible();
            return IsSolvable();
        }

        private bool IsSolvable()
        {
            Tnode current;
            Queue<Tnode> q = new Queue<Tnode>();
            HashSet<Tnode> visited = new HashSet<Tnode>(new Tnode.EqualityComparer());

            q.Enqueue(start);
            visited.Add(start);
            while (q.Count != 0)
            {
                current = q.Dequeue();
                foreach (var newNode in Neighbours(current))
                {
                    if (newNode == finish) return true;
                    if (!visited.Contains(newNode))
                    {
                        q.Enqueue(newNode);
                        visited.Add(newNode);
                    }
                }
            }

            return false;
        }

        private bool AddDoor()
        {
            Tnode[] inaccessible = GetInaccessible();
            foreach (var n in inaccessible)
            {
                if (!compatDoor.ContainsKey(map[n].name) || map[n].name.Contains(".")) continue;
                if (MakeDoor(n)) return true;
            }

            return false;
        }

        private bool MakeDoor(Tnode n)
        {
            Utils.Assert(compatDoor.ContainsKey(map[n].name));
            Tstate state = map[n];
            for (int i = 0; i < 4; i++)
                if (!state.IsPassing(i))
                {
                    var newNode = n.GetNeighbourByIndex(i);
                    if (newNode == null || !compatDoor.ContainsKey(map[newNode].name)) continue;

                    var baseStatesForN =
                        stateDic[compatDoor[map[n].name][0]].Variants(); // TODO rnd the choice of the element
                    var baseStatesForNewNode = stateDic[compatDoor[map[newNode].name][0]].Variants();

                    // find rotation of matching state for n
                    Tstate NTstate = null;
                    foreach (var t in baseStatesForN)
                    {
                        if (CompatExcept(n, t, i))
                        {
                            NTstate = t;
                            break;
                        }
                    }

                    if (NTstate == null) return false;

                    // find rotation of matching state for newNode
                    Tstate NewNodeTstate = null;
                    foreach (var t in baseStatesForNewNode)
                    {
                        if (CompatExcept(newNode, t, (i + 2) % 4))
                        {
                            NewNodeTstate = t;
                            break;
                        }
                    }

                    if (NewNodeTstate == null) return false;

                    // apply the changes
                    map[n] = NTstate;
                    map[newNode] = NewNodeTstate;
                    Debug.DrawLine(n.Pos * 2 + new Vector3(0, 4, 0), newNode.Pos * 2 + new Vector3(0, 4, 0), Color.red);
                    return true;
                }

            return false;
        }

        private bool CompatExcept(Tnode n, Tstate s, int dir)
        {
            for (int i = 0; i < 4; i++)
            {
                if (i == dir) continue;
                Tnode neighbour = n.GetNeighbourByIndex(i);
                Tstate neighbourState;
                if (neighbour == null) neighbourState = outside;
                else neighbourState = map[neighbour];
                if (!s.IsCompatible(i, neighbourState, ifaceDic)) return false;
            }

            return true;
        }

        private (Tnode, Tnode)[] FindDoors()
        {
            List<Tnode> doors = new List<Tnode>();
            List<(Tnode, Tnode)> res = new List<(Tnode, Tnode)>();
            foreach (var v in map)
            {
                if (v.Value.name.Contains("door") && !v.Value.name.Contains("ext")) doors.Add(v.Key);
            }

            while (doors.Count != 0)
            {
                Tnode n = doors[0];
                doors.RemoveAt(0);
                for (int i = 0; i < doors.Count; i++)
                {
                    if (CoDoor(n, doors[i]))
                    {
                        res.Add((n, doors[i]));
                        doors.RemoveAt(i);
                    }
                }
            }

            return res.ToArray();
        }

        private bool MakeWall(Tnode a, Tnode b)
        {
            Utils.Assert(compatWall.ContainsKey(map[a].name));
            Utils.Assert(compatWall.ContainsKey(map[b].name));
            for (int i = 0; i < 4; i++)
            {
                if (a.GetNeighbourByIndex(i) != b) continue;
                var baseStatesForA =
                    stateDic[compatWall[map[a].name][0]].Variants(); // TODO rnd the choice of the element
                var baseStatesForB = stateDic[compatWall[map[b].name][0]].Variants();

                // find rotation of matching state for A
                Tstate AState = null;
                foreach (var t in baseStatesForA)
                {
                    if (CompatExcept(a, t, i))
                    {
                        AState = t;
                        break;
                    }
                }

                // find rotation of matching state for B
                Tstate BState = null;
                foreach (var t in baseStatesForB)
                {
                    if (CompatExcept(b, t, (i + 2) % 4))
                    {
                        BState = t;
                        break;
                    }
                }

                if (AState == null || BState == null) return false;
                // apply the changes
                map[a] = AState;
                map[b] = BState;
                Debug.DrawLine(a.Pos * 2 + new Vector3(0, 4, 0), b.Pos * 2 + new Vector3(0, 4, 0), Color.green);
                return true;
            }

            return false;
        }

        private bool CoDoor(Tnode a, Tnode b)
        {
            for (int i = 0; i < 4; i++)
            {
                if (a.GetNeighbourByIndex(i) != b) continue;
                if (map[a].iface[i].ToLower().Contains("d")) return true;
            }

            return false;
        }

        private bool IsToClose(Tnode n, Tnode N)
        {
            Queue<Tnode> q = new Queue<Tnode>();
            Queue<Tnode> Q = new Queue<Tnode>();
            HashSet<Tnode> h = new HashSet<Tnode>(new Tnode.EqualityComparer());
            HashSet<Tnode> H = new HashSet<Tnode>(new Tnode.EqualityComparer());

            h.Add(n);
            h.Add(N);
            H.Add(n);
            H.Add(N);

            foreach (var c in Neighbours(n))
            {
                if (!h.Contains(c))
                {
                    q.Enqueue(c);
                    //Debug.DrawLine(n.Pos * 2 + new Vector3(0, 4, 0), c.Pos * 2 + new Vector3(0, 3.5f, 0), Color.magenta);
                }
            }

            q.Enqueue(null);
            foreach (var C in Neighbours(N))
            {
                if (!H.Contains(C))
                {
                    Q.Enqueue(C);
                    //Debug.DrawLine(N.Pos * 2 + new Vector3(0, 4, 0), C.Pos * 2 + new Vector3(0, 3.5f, 0), Color.cyan);
                }
            }

            Q.Enqueue(null);

            int max = 4;
            int i = 0;
            int I = 0;

            while (i + I < max && q.Count != 0 && Q.Count != 0)
            {
                Tnode c = q.Dequeue();
                Tnode C = Q.Dequeue();

                if (c == null)
                {
                    i++;
                    q.Enqueue(null);
                }
                else
                {
                    if (H.Contains(c))
                    {
                        //Debug.DrawLine(c.Pos * 2 + new Vector3(0, 5, 0), c.Pos * 2 + new Vector3(0, 3.5f, 0), Color.cyan);
                        return true;
                    }

                    foreach (var new_n in Neighbours(c))
                        if (!h.Contains(new_n))
                        {
                            q.Enqueue(new_n);
                            h.Add(new_n);
                            //Debug.DrawLine(c.Pos * 2 + new Vector3(0, 4, 0), new_n.Pos * 2 + new Vector3(0, 3.5f, 0), Color.magenta);
                        }
                }

                if (C == null)
                {
                    I++;
                    Q.Enqueue(null);
                }
                else
                {
                    if (h.Contains(C))
                    {
                        //Debug.DrawLine(C.Pos * 2 + new Vector3(0, 5, 0), C.Pos * 2 + new Vector3(0, 3.5f, 0), Color.magenta);
                        return true;
                    }

                    foreach (var new_N in Neighbours(C))
                        if (!H.Contains(new_N))
                        {
                            Q.Enqueue(new_N);
                            H.Add(new_N);
                            //Debug.DrawLine(C.Pos * 2 + new Vector3(0, 4, 0), new_N.Pos * 2 + new Vector3(0, 3.5f, 0), Color.cyan);
                        }
                }
            }

            return false;
        }

        private Tnode[] GetInaccessible()
        {
            Tnode current;
            Queue<Tnode> q = new Queue<Tnode>();
            HashSet<Tnode> visited = new HashSet<Tnode>(new Tnode.EqualityComparer());

            bool hasNeighbours = false;

            q.Enqueue(start);
            visited.Add(start);
            while (q.Count != 0)
            {
                current = q.Dequeue();
                foreach (var newNode in Neighbours(current))
                {
                    if (!visited.Contains(newNode))
                    {
                        q.Enqueue(newNode);
                        visited.Add(newNode);
                    }
                }
            }

            HashSet<Tnode> all = new HashSet<Tnode>(map.Keys, new Tnode.EqualityComparer());
            all.ExceptWith(visited);
            return all.ToArray();
        }
        
        private void RmInaccessible()
        {
            Tnode current;
            Queue<Tnode> q = new Queue<Tnode>();
            HashSet<Tnode> visited = new HashSet<Tnode>(new Tnode.EqualityComparer());

            bool hasNeighbours = false;

            q.Enqueue(start);
            visited.Add(start);
            while (q.Count != 0)
            {
                current = q.Dequeue();
                foreach (var newNode in Neighbours(current))
                {
                    if (!visited.Contains(newNode))
                    {
                        q.Enqueue(newNode);
                        visited.Add(newNode);
                    }
                }
            }

            HashSet<Tnode> all = new HashSet<Tnode>(map.Keys, new Tnode.EqualityComparer());
            all.ExceptWith(visited);
            foreach (var n in all)
            {
                map[n].models = new List<Model>();
                map[n].options = new Option(0, true);
            }
        }

        private List<Tnode> Neighbours(Tnode n)
        {
            List<Tnode> res = new List<Tnode>();
            if (n.E != null && map[n].IsPassing(0)) res.Add(n.E);
            if (n.N != null && map[n].IsPassing(1)) res.Add(n.N);
            if (n.W != null && map[n].IsPassing(2)) res.Add(n.W);
            if (n.S != null && map[n].IsPassing(3)) res.Add(n.S);
            return res;
        }
    }
}