using System;
using System.Collections.Generic;
using System.Text;

namespace Map.WFC
{
    public class Option
    {
        List<Model> models = new List<Model>();
        List<Option> options = new List<Option>();
        public float proba;
        public bool BR;

        public Option(float proba, bool BR)
        {
            this.proba = proba;
            this.BR = BR;
        }

        public void AddModel(Model m)
        {
            models.Add(m);
        }

        public void AddOptions(Option o)
        {
            options.Add(o);
        }

        public (List<Model>, bool) Resolve(Random rnd)
        {
            List<Model> m_list = new List<Model>();
            foreach (var m in models)
            {
                m_list.Add(m);
            }
            
            foreach (var o in options)
            {
                if (o.proba < rnd.NextDouble()) continue;
                var t = o.Resolve(rnd);
                m_list.AddRange(t.Item1);
                if (t.Item2) break;
            }
            return (m_list, BR);
        }
    }
}