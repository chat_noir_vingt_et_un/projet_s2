using System;
using UnityEngine;

namespace Map.WFC
{
    public class Model
    {
        public string path;
        public Vector3 pos;
        public int rotation;
        public Vector3 scale;
        public bool collider;
        
        public Model(string s)
        {
            string[] input = s.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
            path = input[0];
            pos = Utils.ChgtConvention(Utils.ParseV3(input[1]));
            rotation = int.Parse(input[2]);
            scale = Utils.ChgtConvention(Utils.ParseV3(input[3]));
            collider = input[4] == "o";
        }

        public Model(Model m)
        {
            path = m.path;
            pos = m.pos;
            rotation = m.rotation;
            scale = m.scale;
            collider = m.collider;
        }

        public override string ToString()
        {
            //return $"{path} {Utils.Repr(Utils.ChgtConvention(pos))} {rotation}";
            return $"{path} {Utils.Repr(Utils.ChgtConvention(pos))} {rotation} {Utils.Repr(Utils.ChgtConvention(scale))} {(collider ? 'o' : 'x')}";
        }
    }
}