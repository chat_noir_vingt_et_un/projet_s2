﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    private EnemyManager e;

    private float timeUntilCongrats;
    private float timer;

    private bool isdead;
    // Start is called before the first frame update
    void Start()
    {
        e = GetComponent<EnemyManager>();
        timeUntilCongrats = 4f;
        timer = 0f;
        isdead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isdead)
        {
            if (e.Health == 0)
            {
                isdead = true;
            }
        }
        else
        {
         
            timer += Time.deltaTime;
            if (timer > timeUntilCongrats)
            {
                PhotonNetwork.LeaveRoom();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                PhotonNetwork.LoadLevel("Congratulations");
            }
        }
    }
}
